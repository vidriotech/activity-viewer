import os.path as op

import setuptools

readme = op.join(op.abspath(op.dirname(__file__)), "README.rst")
with open(readme, "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="activity-viewer",
    version="0.1.0",
    packages=setuptools.find_packages(),
    url="https://gitlab.com/aliddell/activity-viewer",
    license="MIT",
    author="Alan Liddell",
    author_email="alan@vidriotech.com",
    description="Like the MouseLight Neuron Browser, but with ephys visualizations",
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)
