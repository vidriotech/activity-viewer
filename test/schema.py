import os.path as op
import json

from collections import OrderedDict

import graphene

from activity_viewer.graphql.schema import (BrainArea, Error, InputPosition, Neuron,
                                            Node, Predicate, QueryOperator, Sample,
                                            SearchContext, SearchOutput, StructureIdentifier,
                                            SystemSettings, Tracing, TracingStructure)

ASSET_BASE = op.join(op.dirname(__file__), "assets")

# brain areas
all_brain_areas = []
with open(op.join(ASSET_BASE, "brain_areas.json"), "r") as io:
    brain_areas = json.load(io)

    for val in brain_areas:
        all_brain_areas.append(BrainArea(id=val["id"],
                                         name=val["name"],
                                         acronym=val["acronym"],
                                         aliases=val["aliases"],
                                         structure_id=val["structureId"],
                                         depth=val["depth"],
                                         parent_structure_id=val["parentStructureId"],
                                         structure_id_path=val["structureIdPath"],
                                         geometry_color=val["geometryColor"],
                                         geometry_file=val["geometryFile"],
                                         geometry_enable=val["geometryEnable"]))

# neurons
all_neurons = []
with open(op.join(ASSET_BASE, "neurons.json"), "r") as io:
    neurons = json.load(io)

    for val in neurons:
        tracings = []
        for tracing in val["tracings"]:
            tracings.append(Tracing(id=tracing["id"],
                                    tracing_structure=TracingStructure(id=tracing["tracingStructure"]["id"],
                                                                       name=tracing["tracingStructure"]["name"],
                                                                       value=tracing["tracingStructure"]["value"]),
                                    soma=Node(id=tracing["soma"]["id"],
                                              x=tracing["soma"]["x"],
                                              y=tracing["soma"]["y"],
                                              z=tracing["soma"]["z"],
                                              radius=tracing["soma"]["radius"],
                                              parent_number=tracing["soma"]["parentNumber"],
                                              sample_number=tracing["soma"]["sampleNumber"],
                                              brain_area_id=tracing["soma"]["brainAreaId"],
                                              structure_identifier_id=tracing["soma"]["structureIdentifierId"])))
        all_neurons.append(Neuron(id=val["id"],
                                  id_string=val["idString"],
                                  brain_area=BrainArea(val["brainArea"]["id"], val["brainArea"]["acronym"]) if val["brainArea"] else None,
                                  sample=Sample(id=val["sample"]["id"],
                                                id_number=val["sample"]["idNumber"]),
                                  tracings=tracings))

# query operators
all_query_operators = []
with open(op.join(ASSET_BASE, "query_operators.json"), "r") as io:
    query_operators = json.load(io)

    for val in query_operators:
        all_query_operators.append(QueryOperator(id=val["id"],
                                                 display=val["display"],
                                                 operator=val["operator"]))

# structure identifiers
all_structure_identifiers = []
with open(op.join(ASSET_BASE, "structure_identifiers.json"), "r") as io:
    structure_identifiers = json.load(io)

    for val in structure_identifiers:
        all_structure_identifiers.append(StructureIdentifier(id=val["id"],
                                                                name=val["name"],
                                                                value=val["value"]))

# system settings
with open(op.join(ASSET_BASE, "system_settings.json"), "r") as io:
    val = json.load(io)

    system_settings = SystemSettings(api_version=val["apiVersion"],
                                        api_release=val["apiRelease"],
                                        neuron_count=val["neuronCount"])

# tracing structures
all_tracing_structures = []
with open(op.join(ASSET_BASE, "tracing_structures.json"), "r") as io:
    tracing_structures = json.load(io)

    for val in tracing_structures:
        all_tracing_structures.append(TracingStructure(id=val["id"],
                                                       name=val["name"],
                                                       value=val["value"]))


class Query(graphene.ObjectType):
    brain_areas = graphene.Field(graphene.List(graphene.NonNull(BrainArea), required=True))
    query_operators = graphene.Field(graphene.List(graphene.NonNull(QueryOperator), required=True))
    search_neurons = graphene.Field(SearchOutput, args={"context": SearchContext()})
    structure_identifiers = graphene.Field(graphene.List(graphene.NonNull(StructureIdentifier), required=True))
    system_message = graphene.Field(graphene.String)
    system_settings = graphene.Field(SystemSettings, args={"search_scope": graphene.Int()})
    tracing_structures = graphene.Field(graphene.List(graphene.NonNull(TracingStructure), required=True))

    def resolve_brain_areas(self, info):
        return all_brain_areas

    def resolve_query_operators(self, info):
        return all_query_operators

    def resolve_search_neurons(self, info, context):
        # return OrderedDict([("neurons", all_neurons),
        #                     ("total_count", len(all_neurons)),
        #                     ("query_time", 236),
        #                     ("nonce", "cjw285em400002764w6u9a9yc")])
        return SearchOutput(neurons=all_neurons,
                            total_count=len(all_neurons),
                            query_time=236,
                            nonce=context.nonce)

    def resolve_structure_identifiers(self, info):
        return all_structure_identifiers

    def resolve_system_message(self, info):
        return "Feelin' fine"

    def resolve_system_settings(self, info, search_scope):
        return system_settings # for test purposes, always public search scope

    def resolve_tracing_structures(self, info):
        return all_tracing_structures

test_schema = graphene.Schema(query=Query)
