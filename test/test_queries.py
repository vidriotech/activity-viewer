import os.path as op
import sys
sys.path.insert(0, op.abspath(op.join(op.dirname(__file__), "..")))

import requests

from graphene.test import Client

from activity_viewer.graphql import queries
from test.schema import test_schema

client = Client(test_schema)


def test_constants_query():
    # call from the real server
    response = client.execute(queries.STRUCTURES_QUERY)
    print(response)


print(client.execute(queries.STRUCTURES_QUERY, variables={"searchScope": 7})["data"]["structureIdentifiers"])
ctxt = {"scope": 7, "nonce": "foobar", "predicates": []}
print(client.execute(queries.SEARCH_NEURONS_QUERY, variables={"context": ctxt}))
