import json
import hashlib
import numpy as np

import os.path as op
import sys
sys.path.insert(0, op.realpath(op.join(op.realpath(__file__), "..", "..")))

from activity_viewer.constants import JET_MAP


def setup_module():
    hasher = hashlib.sha256()
    point_models = []

    for i in range(100):
        x = 6000*np.random.sample() + 3000
        y = 6000*np.random.sample() + 1000
        z = 12000*np.random.sample() + 2000
        hasher.update(np.array([x, y, z]))

        time_varies = np.random.binomial(1, 0.5)
        if time_varies == 1:
            max_t = np.random.choice(9900) + 100
            n_steps = np.random.choice(np.arange(50, max_t))
            timesteps = np.arange(0, max_t, max_t / n_steps).astype(np.int64).tolist()
        else:
            timesteps = None

        color_varies = time_varies*np.random.binomial(1, 0.3)
        if color_varies == 1:
            c = np.random.choice(256, 3)
            color = []
            coeffs = np.array([1, 1, 1])

            for j in range(len(timesteps)):
                color.append("#{0:02x}{1:02x}{2:02x}".format(*c))
                assert len(color[-1]) == 7
                add = np.random.binomial(1, 0.5, 3)
                coeffs[c == 255] = -1
                coeffs[c == 0] = 1
                c += add * coeffs
        else:
            color = np.random.choice(JET_MAP).lower()

        radius_varies = time_varies * np.random.binomial(1, 0.3)
        if radius_varies == 1:  # radius varies
            radius = (200 + 100 * np.sin(np.arange(len(timesteps)))).tolist()
        else:
            radius = 100 + 200 * np.random.random()

        point_models.append(dict(id=hasher.hexdigest(), x=x, y=y, z=z, t=timesteps, color=color, radius=radius))

    with open(op.join(op.dirname(op.realpath(__file__)), "assets", "example_points.json"), "w") as fh:
        json.dump(point_models, fh, indent=4)


