#!/usr/bin/env python

import os.path as op
import sys
sys.path.insert(0, op.realpath(op.join(op.realpath(__file__), "..", "..")))  # dir containing test/ & activity_viewer/

from flask import Flask
from flask_graphql import GraphQLView

from test.schema import test_schema

ASSETS_DIR = op.join(op.dirname(op.abspath(__file__)), "assets")

app = Flask(__name__)
app.add_url_rule("/graphql", view_func=GraphQLView.as_view("graphql", schema=test_schema, graphiql=True))


@app.route("/<path:subpath>")
def get_asset(subpath: str):
    if subpath.endswith(".obj"):
        full_filename = op.join(ASSETS_DIR, "allen", "obj", subpath)
    else:
        full_filename = op.join(ASSETS_DIR, subpath)

    print(full_filename)
    if op.isfile(full_filename):
        with open(full_filename, "r") as io:
            return io.read()
    return f"Could not find asset {subpath}", 404


if __name__ == "__main__":
    app.run()
