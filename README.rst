activity-viewer
===============

A collection of Jupyter widgets for visualizing ephys unit activity.

Installation
------------

**Note:** previous versions of this README had you install this software via a `conda` environment.
For now we recommend you use a `virtualenv`_.

1. `Clone this repository <https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository>`__ to your machine.
2. `Install virtualenv <https://virtualenv.pypa.io/en/stable/installation/>`__. If you have `Anaconda`_ installed, you should already have ``virtualenv``, so we recommend you install Anaconda. Test that you have ``virtualenv`` by running ``virtualenv --version`` in your favorite terminal. You should see a version number, e.g., ``16.7.3``.
3. Open a terminal (on Windows, use the Anaconda Prompt if you have it) and ``cd`` to the directory containing this README.
4. Run ``virtualenv venv``. This creates a folder called ``venv`` in this directory containing activation scripts and, eventually, a container library for this package.
5. Now you need to invoke the virtualenv activate script. Call ``source venv/bin/activate`` on Unix systems. On Windows, if you're using the Anaconda Prompt or CMD.exe, run ``venv\Scripts\activate.bat``. If using Powershell, call ``. venv\Scripts\activate``. (Note the ``.`` character is part of the command.) You should now see ``(venv)`` next to your prompt. (To deactivate the environment, simply run ``deactivate``.)
6. Run ``pip install -r requirements.txt``. The install process is pretty lengthy, but should not affect any other Python environment on your system.
7. Making sure ``(venv)`` is still in your prompt, run ``python setup.py develop``. This will install ``activity-viewer`` to your virtualenv so you won't need to fiddle with ``sys.path`` to import its submodules.

Any time you need to activate this environment, do steps 3 and 5.

Usage
-----

**Note:** we've tested this software using Python 3.6. Please report any problems by opening an issue `here <https://gitlab.com/aliddell/activity-viewer/issues/new>`__.

Getting started
```````````````

Once you've activated your environment (see the `Installation section <#Installation>`_), run ``jupyter notebook`` and create a new notebook.

Importing points
````````````````

You'll need some points you want to display, and they need to be in the Allen CCF (2017 version, which is the latest as of this commit).
How you transform electrode locations on a probe shank into the Allen CCF is your business for now.
Each of these points can have display quantities, namely size (radius), color, and opacity, which correspond to various properties of that unit, e.g., spike rate.
However you want to map them is also up to you, as long as you're consistent.
These values may also vary over time and so represent time-varying quantities.
A group of points along a single shank during a single penetration is represented by a ``PenetrationGroup``.
To create a ``PenetrationGroup``, organize your points into a JSON file formatted like so:

.. code-block:: json

    {
        "id": "a unique id string",
        "points": [
            {
                "x": 100,
                "y": 100,
                "z": 100,
                "alpha": 0.8,  // alpha can be constant or vary over time (this is constant, but it would be similar to color below)
                "color": {
                    "t": [0, 1, 2],  // time steps, in seconds
                    "vals": ["#ff0000", "#00ff00", "#0000ff"]
                },
                "radius": {
                    "t": [0, 2, 3],
                    "vals": [200.0, 300.0, 250.0]
                }
            },
            ...  // repeat with as many points as are in your penetration group
        ]
    }

**A single JSON file is assumed to correspond to a single penetration group.**

The ``id`` field should be a string which is unique in the space of all penetration groups you intend to display at once.
The ``points`` field should be an array of ``Point`` objects, with the following fields:

- ``x``, ``y``, ``z``: From the AI: "The common reference space is in PIR orientation, where x axis = Anterior-to-Posterior, y axis = Superior-to-Inferior and z axis = Left-to-Right."
- ``alpha``: a numeric value between 0 and 1 (inclusive on either end).
- ``color``: an RGB hex value (case insensitive), beginning with the ``#`` character. You can use ``matplotlib.colors.rgb2hex`` to find this, for example:

.. code-block:: python

    from matplotlib.colors import rgb2hex
    hex_white = rgb2hex([1., 1., 1.])  # returns '#ffffff'

will give you the hex value for white.

- ``radius``: a radius for the point in microns.

Any or all of these last three values (``alpha``, ``color``, or ``radius``) can vary over time, in which case substitute the scalar value with an object formatted like so:

.. code-block:: json

    {
        "t": [...],
        "vals": [...]
    }

where ``vals[i]`` refers to the value that this point takes on at time ``t[i]``.

The easiest way to create a ``PenetrationGroup`` is to use the ``from_json_file`` function from ``activity_viewer.models.point``, e.g.,

.. code-block:: python

    from activity_viewer.models.point import from_json_file
    my_penetration_group = from_json_file("/path/to/my/group_file.json")

A ``PenetrationGroup`` can be added to one or more ``Viewer`` objects (`see below <#Displaying Points>`__).

Displaying points
`````````````````

For right now there are two basic viewers, the ``SliceViewer`` and the ``CompartmentViewer``.
The ``SliceViewer`` displays points from a given ``PenetrationGroup`` embedded within a "pseudocoronal" (i.e., rotated with respect to the y-z plane) slice, both of the reference and annotation volumes:

.. image:: docs/source/.static/sv.png
    :scale: 50%
    :align: center

**Labeling the individual compartments in the reference slice is a TODO.**

The ``CompartmentViewer`` will be familiar to users of the `MouseLight Neuron Browser`_.

.. image:: docs/source/.static/cv.png
    :scale: 75%
    :align: center

To create either of these, it's necessary to create an ``APIEndpoint`` and use that as an argument to the constructor, like so:

.. code-block:: python

    import activity_viewer
    import activity_viewer.viewers

    endpoint = activity_viewer.models.APIEndpoint()
    slice_viewer = activity_viewer.viewers.SliceViewer(endpoint, resolution=25)  # resolution is in um/pix and can be one of 10, 25, 50, or 100
    compartment_viewer = activity_viewer.viewers.CompartmentViewer(endpoint)

Then you can display your penetration group by adding it, like so:

.. code-block:: python

    slice_viewer.add_penetration_group(my_penetration_group)
    compartment_viewer.add_penetration_group(my_penetration_group)


See the `demo notebook <notebooks/2019-08-23-demo.ipynb>`__ for examples

Acknowledgements
----------------

OpenGL vertex and fragment shaders thanks to `Patrick Edson <https://github.com/pedson>`__, as seen on the
`MouseLight Neuron Browser`_.
Example data provided by `Liu Liu <https://www.janelia.org/people/liu-liu-0>`__.

.. _Anaconda: https://www.anaconda.com/
.. _MouseLight Neuron Browser: http://ml-neuronbrowser.janelia.org/
.. _virtualenv: https://virtualenv.pypa.io/en/stable/