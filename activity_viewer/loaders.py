from copy import deepcopy
import re

import numpy as np

import pythreejs

object_pattern = re.compile(r"^[og]\s*(.+)?")
material_library_pattern = re.compile(r"^mtllib ")
material_use_pattern = re.compile(r"^usemtl ")


def compute_vertex_normals(positions):  # exec_three_obj_method("computeVertexNormals") seems to fail here
    """Simple implementation of vertex normal calculation."""
    normals = np.empty_like(positions)

    for i in range(0, positions.size, 9):
        pa = positions[i:i+3]
        pb = positions[i+3:i+6]
        pc = positions[i+6:i+9]

        cb = np.cross(pc - pb, pa - pb)
        cb /= np.linalg.norm(cb) # normalize

        normals[i:i+3] = cb
        normals[i+3:i+6] = cb
        normals[i+6:i+9] = cb

    return normals


class ParserStateObject:
    """Adapted from https://github.com/mrdoob/three.js/blob/master/examples/js/loaders/OBJLoader.js"""
    def __init__(self, name=None, from_declaration=False):
        self.name = name if name else ""
        self.from_declaration = from_declaration is not False
        self.geometry = {"vertices": [],
                         "normals": [],
                         "colors": [],
                         "uvs": [],
                         "type": ""}

        self.materials = []
        self.smooth = True

    def start_material(self, name, libraries):
        previous = self._finalize(False)

        # New usemtl declaration overwrites an inherited material, except if faces were declared
        # after the material, then it must be preserved for proper MultiMaterial continuation.
        if previous and (previous["inherited"] or previous["group_count"] <= 0):
            self.materials.remove(previous)

        material = {"name": name if name else "",
                    "mtllib": libraries[-1] if (type(libraries) == "list" and len(libraries) > 0) else "",
                    "smooth": previous["smooth"] if previous is not None else self.smooth,
                    "group_start": previous["group_end"] if previous is not None else False,
                    "group_end": -1,
                    "group_count": -1,
                    "inherited": False}

        self.materials.append(material)

    def current_material(self):
        if len(self.materials) > 0: # otherwise returns None
            return self.materials[-1]

    def _finalize(self, end):
        last_multi_material = self.current_material()

        if last_multi_material and last_multi_material["group_end"] == - 1:
            last_multi_material["group_end"] = len(self.geometry["vertices"]) / 3
            last_multi_material["group_count"] = last_multi_material["group_end"] - last_multi_material["group_start"]
            last_multi_material["inherited"] = False

        # Ignore objects tail materials if no face declarations followed them before a new o/g started.
        if end and len(self.materials) > 1:
            for mi in range(len(self.materials), 0, -1):
                if self.materials[mi-1]["group_count"] <= 0:
                    self.materials.pop(mi-1)

        # Guarantee at least one empty material, this makes the creation later more straight forward.
        if end and len(self.materials) == 0:
            self.materials.append({"name": "", "smooth": self.smooth})

        return last_multi_material


class ParserState:
    """Adapted from https://github.com/mrdoob/three.js/blob/master/examples/js/loaders/OBJLoader.js"""
    def __init__(self):
        self.objects = []
        self.object = None

        self.vertices = []
        self.normals = []
        self.colors = []
        self.uvs = []

        self.material_libraries = []

        self.start_object("", False)

    def start_object(self, name, from_declaration=None):
        if self.object is not None and self.object.from_declaration is False:
            self.object.name = name
            self.object.from_declaration = (from_declaration is False)
            return

        previous_material = self.object.current_material() if self.object is not None else None
        if self.object is not None:
            self.object._finalize(True)

        self.object = ParserStateObject()

        # Inherit previous objects material.
        # Spec tells us that a declared material must be set to all objects until a new material is declared.
        # If a usemtl declaration is encountered while this new object is being parsed, it will
        # overwrite the inherited material. Exception being that there was already face declarations
        # to the inherited material, then it will be preserved for proper MultiMaterial continuation.
        if previous_material is not None and "name" in previous_material:
            declared = deepcopy(previous_material)
            declared["inherited"] = True
            self.object.materials.append(declared)

        self.objects.append(self.object)

    def finalize(self):
        if self.object is not None:
            self.object._finalize(True)

    def parse_vertex_index(self, value, length):
        index = int(value)
        return (index - 1 if index >= 0 else index + length / 3)*3

    def parse_normal_index(self, value, length):
        return self.parse_vertex_index(value, length)

    def parse_uv_index(self, value, length):
        index = int(value)
        return (index - 1 if index >= 0 else index + length / 2)*2

    def _add_object(self, key, *args):
        src = self.__getattribute__(key)
        dst = self.object.geometry[key]

        for arg in args:
            dst += [src[arg + 0], src[arg + 1], src[arg + 2]]

    def add_vertex(self, a, b, c):
        self._add_object("vertices", a, b, c)
#            src = self.vertices
#            dst = self.object.geometry["vertices"]
#
#            dst += [src[a + 0], src[a + 1], src[a + 2]]
#            dst += [src[b + 0], src[b + 1], src[b + 2]]
#            dst += [src[c + 0], src[c + 1], src[c + 2]]

    def add_vertex_point(self, a):
        self._add_object("vertices", a)
#            src = self.vertices
#            dst = self.object.geometry["vertices"]
#
#            dst += [src[a + 0], src[a + 1], src[a + 2]]

    def add_vertex_line(self, a):
        self._add_object("vertices", a)

    def add_normal(self, a, b, c):
        self._add_object("normals", a, b, c)
#            src = self.normals
#            dst = self.object.geometry["normals"]
#
#            dst += [src[a + 0], src[a + 1], src[a + 2]]
#            dst += [src[b + 0], src[b + 1], src[b + 2]]
#            dst += [src[c + 0], src[c + 1], src[c + 2]]

    def add_color(self, a, b, c):
        self._add_object("colors", a, b, c)
#            src = self.colors
#            dst = self.object.geometry["colors"]
#
#            dst += [src[a + 0], src[a + 1], src[a + 2]]
#            dst += [src[b + 0], src[b + 1], src[b + 2]]
#            dst += [src[c + 0], src[c + 1], src[c + 2]]

    def add_uv(self, a, b, c):
        self._add_object("uvs", a, b, c)
#            src = self.uvs
#            dst = self.object.geometry["uvs"]
#
#            dst += [src[a + 0], src[a + 1], src[a + 2]]
#            dst += [src[b + 0], src[b + 1], src[b + 2]]
#            dst += [src[c + 0], src[c + 1], src[c + 2]]

    def add_uv_line(self, a):
        self._add_object("uvs", a)

    def add_face(self, a, b, c, ua, ub, uc, na, nb, nc):
        vlen = len(self.vertices)

        ia = self.parse_vertex_index(a, vlen)
        ib = self.parse_vertex_index(b, vlen)
        ic = self.parse_vertex_index(c, vlen)

        self.add_vertex(ia, ib, ic)

        if ua is not None and na is not None:
            # Normals are many times the same. If so, skip function call.
            nlen = len(self.normals)
            ia = self.parse_normal_index(na, nlen)
            ib = ia if na == nb else self.parse_normal_index(nb, nlen)
            ic = ia if na == nc else self.parse_normal_index(nc, nlen)

            self.add_normal(ia, ib, ic)

        if len(self.colors) > 0:
            self.add_color(ia, ib, ic)

    def add_point_geometry(self, vertices):
        self.object.geometry["type"] = "Points"

        vlen = len(self.vertices)
        for v in vertices:
            self.add_vertex_point(self.parse_vertex_index(v, vlen))

    def add_line_geometry(self, vertices, uvs):
        self.object.geometry["type"] = "Line"

        vlen = len(self.vertices)
        for v in vertices:
            self.add_vertex_line(self.parse_vertex_index(v, vlen))

        uvlen = len(self.uvs)
        for uv in uvs:
            self.add_uv_line(self.parse_uv_index(uv, uvlen))


class OBJParser:
    def __init__(self):
        self.materials = None

    def parse(self, text):
        state = ParserState()

        # join lines separated by a \
        text = text.replace("\\\n", "")

        # split into lines
        lines = text.splitlines()

        for line in lines:
            line = line.lstrip()
            if not line:
                continue

            if line.startswith("#"):
                continue

            if line.startswith("v"): # vertices, normals, uvs
                data = line.split()

                if data[0] == "v":
                    state.vertices += [float(data[1]), float(data[2]), float(data[3])]

                    if len(data) == 8:
                        state.colors += [float(data[4]), float(data[5]), float(data[6])]
                elif data[0] == "vn":
                    state.normals += [float(data[1]), float(data[2]), float(data[3])]
                elif data[0] == "vt":
                    state.uvs += [float(data[1]), float(data[2])]
            elif line.startswith("f"): # faces
                line_data = line[1:].strip()
                vertex_data = line_data.split()
                face_vertices = []

                # Parse the face vertex data into an easy to work with format
                for vertex in vertex_data:
                    if vertex:
                        face_vertices.append(vertex.split("/"))

                # Draw an edge between the first vertex and all subsequent vertices to form an n-gon
                v1 = face_vertices[0]
                for j in range(1, len(face_vertices)-1):
                    v2 = face_vertices[j]
                    v3 = face_vertices[j+1]

                    state.add_face(v1[0], v2[0], v3[0],
                                   v1[1] if len(v1) > 1 else None, v2[1] if len(v2) > 1 else None, v3[1] if len(v3) > 1 else None,
                                   v1[2] if len(v1) > 2 else None, v2[2] if len(v2) > 2 else None, v3[2] if len(v3) > 2 else None)

            elif line.startswith("l"):
                line_parts = line[1:].strip().split()
                line_vertices = []
                line_uvs = []

                if "/" not in line:
                    line_vertices = line_parts
                else:
                    for lp in line_parts:
                        parts = lp.split("/")

                        if parts[0]:
                            line_vertices.append(parts[0])
                        if parts[1]:
                            line_uvs.append(parts[1])

                state.add_line_geometry(line_vertices, line_uvs)
            elif line.startswith("p"):
                point_data = line[1:].strip().split()
                state.add_point_geometry(point_data)
            elif line.startswith("s"):
                result = line.split()
                if len(result) > 1:
                    value = result[1].strip().lower()
                    state.object.smooth = (value != "0" and value != "off")
                else:
                    state.object.smooth = True

                material = state.object.current_material()

                if material:
                    material.smooth = state.object.smooth
            else:
                m_obj = object_pattern.match(line)
                m_lib = material_library_pattern.match(line) if not m_obj else None
                m_use = material_use_pattern.match(line) if (not m_obj and not m_lib) else None

                if m_obj:
                    # o object_name
                    # or
                    # g group_name
                    name = m_obj.group()
                    state.start_object(name)
                elif m_lib:
                    # mtl file
                    state.material_libraries.append(line[7:].strip())
                elif m_use:
                    state.object.start_material(line[7:].strip(), state.material_libraries)
                elif line == "\0":
                    continue
                else:
                    raise ValueError("Unexpected line: " + line)

        state.finalize()

        container = pythreejs.Group()
        container.materialLibraries = sum(state.material_libraries, [])

        for obj in state.objects:
            geometry = obj.geometry
            materials = obj.materials
            is_line = geometry["type"] == "Line"
            is_points = geometry["type"] == "Points"
            has_vertex_colors = False

            # Skip o/g line declarations that did not follow with any faces
            if len(geometry["vertices"]) == 0:
                continue

#            buffer_geometry = pythreejs.BufferGeometry()
            position = np.array(geometry["vertices"], dtype=np.float32)

            if len(geometry["normals"]) > 0:
                normal = np.array(geometry["normals"], dtype=np.float32)
                normalized = False
            else:
                normal = compute_vertex_normals(position)
                normalized = True

            buffer_geometry = pythreejs.BufferGeometry(
                        attributes={
                                "position": pythreejs.BufferAttribute(position.reshape((position.size//3, 3)),
                                                                      normalized=False),
                                "normal": pythreejs.BufferAttribute(normal.reshape((normal.size//3, 3)),
                                                                    normalized=normalized)
                                })

            has_vertex_colors = len(geometry["colors"]) > 0
#            if len(geometry["colors"]) > 0:
#                buffer_geometry.attributes["color"] = pythreejs.BufferAttribute(geometry["colors"])
#
#            if len(geometry["uvs"]) > 0:
#                buffer_geometry.attributes["uv"] = pythreejs.BufferAttribute(geometry["uvs"])

            # Create materials
            created_materials = []
            for source_material in materials:
                material = None

                if self.materials is not None:
                    material = self.materials.create(source_material.name) # dubious

                    if is_line and material and not isinstance(material, pythreejs.LineBasicMaterial):
                        material_line = pythreejs.LineBasicMaterial()
                        material_line.copy(material)
                        material_line.lights = False # FIXME
                        material = material_line
                    elif is_points and material and not isinstance(material, pythreejs.PointsMaterial):
                        material_points = pythreejs.PointsMaterial(size=10, sizeAttenuation=False)
                        material_points.copy(material)
                        material = material_points

                if not material:
                    if is_line:
                        material = pythreejs.LineBasicMaterial()
                    elif is_points:
                        material = pythreejs.PointsMaterial()
                    else:
                        material = pythreejs.MeshPhongMaterial()

                    material.name = source_material["name"]

                material.flatShading = False if source_material["smooth"] else True
                material.vertexColors = "VertexColors" if has_vertex_colors else "NoColors"

                created_materials.append(material)

            if len(created_materials) > 1:
                for mi, source_material in enumerate(materials):
                    buffer_geometry.addGroup(source_material["group_start"], source_material["group_count"], mi)

                if is_line:
                    mesh = pythreejs.LineSegments(buffer_geometry, created_materials)
                elif is_points:
                    mesh = pythreejs.Points(buffer_geometry, created_materials)
                else:
                    mesh = pythreejs.Mesh(buffer_geometry, created_materials)
            else:
                if is_line:
                    mesh = pythreejs.LineSegments(buffer_geometry, created_materials[0])
                elif is_points:
                    mesh = pythreejs.Points(buffer_geometry, created_materials[0])
                else:
                    mesh = pythreejs.Mesh(buffer_geometry, created_materials[0])

            mesh.name = obj.name
            container.add(mesh)

        return container
