from datetime import datetime
import os
import os.path as op
import urllib

import json
import pandas as pd

from allensdk.api.queries.mouse_connectivity_api import MouseConnectivityApi
from allensdk.api.queries.ontologies_api import OntologiesApi
from allensdk.core.structure_tree import StructureTree
from allensdk.api.queries.reference_space_api import ReferenceSpaceApi

from activity_viewer.constants import STRUCTURE_CENTERS


class APIEndpoint:
    BUST_AFTER = 3600 * 24 * 90  # bust cache after 90 days

    def __init__(self, ccf_version=MouseConnectivityApi.CCF_VERSION_DEFAULT):
        """A single point of entry for volume, mesh, and image queries."""
        self.ccf_version = ccf_version
        self.cache_dir = op.join(op.expanduser("~"), ".activity-viewer")

        structure_graph_file = op.join(self.cache_dir, "structure_graph.json")
        if op.isfile(structure_graph_file):
            td = (datetime.now() - datetime.fromtimestamp(os.stat(structure_graph_file).st_mtime)).total_seconds()
        else:
            td = self.BUST_AFTER + 1

        if td > self.BUST_AFTER:
            structure_graph = OntologiesApi().get_structures_with_sets([1])  # ID of the adult mouse structure graph
            # This removes some unused fields returned by the query
            structure_graph = StructureTree.clean_structures(structure_graph)
            with open(structure_graph_file, "w") as fh:
                json.dump(structure_graph, fh)

            self._tree = StructureTree(structure_graph)
        else:
            with open(structure_graph_file, "r") as fh:
                self._tree = StructureTree(json.load(fh))

        # serves as a proxy for structures with meshes
        structure_center_file = op.join(self.cache_dir, "structure_centers.csv")
        if op.isfile(structure_center_file):
            td = (datetime.now() - datetime.fromtimestamp(os.stat(structure_center_file).st_mtime)).total_seconds()
        else:
            td = self.BUST_AFTER + 1

        if td > self.BUST_AFTER:
            try:
                self._structure_centers = pd.read_csv(STRUCTURE_CENTERS)
            except urllib.error.HTTPERROR:
                raise ConnectionError("failed to fetch structure centers")

            self._structure_centers.to_csv(structure_center_file)
        else:
            self._structure_centers = pd.read_csv(structure_center_file)

        self._rsa = ReferenceSpaceApi()

    @property
    def ccf_version(self):
        return self._ccf_version

    @ccf_version.setter
    def ccf_version(self, val):
        if not isinstance(val, str):
            raise TypeError("ccf_version should be a string")
        if not val.startswith("annotation/ccf_201"):
            raise ValueError(f"invalid ccf_version '{val}'")

        self._ccf_version = val

    @property
    def cache_dir(self):
        return self._cache_dir

    @cache_dir.setter
    def cache_dir(self, val: str):
        if not op.isdir(val):
            os.makedirs(val, exist_ok=True)
        self._cache_dir = val

    @property
    def reference_space_api(self):
        return self._rsa

    @property
    def structure_centers(self):
        return self._structure_centers

    @property
    def tree(self):
        return self._tree

