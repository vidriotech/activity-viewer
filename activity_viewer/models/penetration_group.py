from collections import defaultdict
import hashlib
import json
from numbers import Real
import re
from typing import Iterable, Tuple, Union

import numpy as np

from activity_viewer.models import Timeseries
from .point_view import TwoView, ThreeView

color_re = re.compile(r"^#[0-9a-f]{6}$", re.I)


class Point:
    def __init__(self, point_id: str, x: Real, y: Real, z: Real, alpha: Timeseries, color: Timeseries,
                 radius: Timeseries, **kwargs):
        """Model of a single point in Allen CCF coordinates, with optionally time-varying display properties.

        Parameters
        ----------
        point_id
        x : Real
            x (AP) coordinate of the point.
        y : Real
            y (DV) coordinate of the point.
        z : Real
            z (LR) coordinate of the point.
        alpha : Timeseries
            Opacity of point.
        color : Timeseries
            Hex-encoded RGB values for point color.
        radius : Timeseries
            Radius of point.
        kwargs : Keyword arguments.
            Other timeseries.
        """
        self._id = self._x = self._y = self._z = self._color = self._radius = self._alpha = None
        self._timeseries = {}

        self.id = point_id
        self.x = x
        self.y = y
        self.z = z
        self.alpha = alpha
        self.color = color
        self.radius = radius

        if "timeseries" in kwargs:
            ts = kwargs["timeseries"]
            if not isinstance(ts, dict):
                raise TypeError(f"timeseries should be a dict, but you gave '{type(ts)}'")

            for key, val in ts.items():
                if not isinstance(val, Timeseries):
                    raise TypeError(f"timeseries with key '{key}' should be a Timeseries, but you gave '{type(val)}'")
                self._timeseries[key] = val

    @property
    def x(self):
        """x coordinate of the point."""
        return self._x

    @x.setter
    def x(self, val):
        if not isinstance(val, Real):
            raise TypeError("x must be a real number")
        self._x = val

    @property
    def y(self):
        """y coordinate of the point."""
        return self._y

    @y.setter
    def y(self, val):
        if not isinstance(val, Real):
            raise TypeError("y must be a real number")
        self._y = val

    @property
    def z(self):
        """z coordinate of the point."""
        return self._z

    @z.setter
    def z(self, val):
        if not isinstance(val, Real):
            raise TypeError("z must be a real number")
        self._z = val

    @property
    def alpha(self):
        return self._alpha

    @alpha.setter
    def alpha(self, aval):
        if isinstance(aval, Real):  # scalar
            aval = Timeseries("alpha", [-np.inf], [aval])
        elif isinstance(aval, dict):
            if len(aval) != 2 or "t" not in aval or "vals" not in aval:
                raise KeyError("setting alpha from dict requires keys 't' and 'vals'")

            # check iterable or at least numeric
            if not isinstance(aval["t"], Iterable) and isinstance(aval["t"], Real):
                aval["t"] = [aval["t"]]
            elif not isinstance(aval["t"], Iterable):
                raise TypeError("times should be an iterable of numeric")

            # check iterable or at least numeric
            if not isinstance(aval["vals"], Iterable) and isinstance(aval["vals"], Real):
                aval["vals"] = [aval["vals"]]
            elif not isinstance(aval["vals"], Iterable):
                raise TypeError("values should be an iterable of numeric")

            aval = Timeseries("alpha", aval["t"], aval["vals"])
        elif not isinstance(aval, Timeseries):
            raise TypeError(f"data type for alpha not understood: '{type(aval)}'")

        # check alpha vals are numeric
        if aval.vals.dtype.kind not in ('i', 'f'):
            raise TypeError(f"alpha values should be numeric, but got '{aval.vals.dtype}'")

        # check in bounds and not all zero
        if ((aval.vals < 0) | (aval.vals > 1)).any():
            raise ValueError("value for alpha given outside [0, 1] bounds")
        elif (aval.vals == 0).all():
            raise ValueError("values for alpha are all zero")

        self._alpha = aval

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, cval):
        if isinstance(cval, str):
            cval = Timeseries("color", [-np.inf], [cval])
        elif isinstance(cval, dict):
            if len(cval) != 2 or "t" not in cval or "vals" not in cval:
                raise KeyError("setting color from dict requires keys 't' and 'vals'")

            # check iterable or at least numeric
            if not isinstance(cval["t"], Iterable) and isinstance(cval["t"], Real):
                cval["t"] = [cval["t"]]
            elif not isinstance(cval["t"], Iterable):
                raise TypeError("times should be an iterable of numeric")

            # check iterable or at least str
            if not isinstance(cval["vals"], Iterable) and isinstance(cval["vals"], str):
                cval["vals"] = [cval["vals"]]
            elif not isinstance(cval["vals"], Iterable):
                raise TypeError("values should be an iterable of str")

            cval = Timeseries("color", cval["t"], cval["vals"])
        else:
            raise TypeError(f"data type for color not understood: '{type(cval)}'")

        # check color vals are str
        if cval.vals.dtype.kind not in ('U', 'S'):
            raise TypeError(f"color values should be str, but got '{cval.vals.dtype}'")

        # check color vals are valid hex colors
        if not (np.fromiter((color_re.match(c) is not None for c in cval.vals), dtype=np.bool)).all():
            raise ValueError("all color values should be hex color codes (e.g., '#FFFFFF', '#8f8f8f')")

        self._color = cval

    @property
    def radius(self):
        return self._radius

    @radius.setter
    def radius(self, rval):
        if isinstance(rval, Real):  # scalar
            rval = Timeseries("radius", [-np.inf], [rval])
        elif isinstance(rval, dict):
            if len(rval) != 2 or "t" not in rval or "vals" not in rval:
                raise KeyError("setting radius from dict requires keys 't' and 'vals'")

            # check iterable or at least numeric
            if not isinstance(rval["t"], Iterable) and isinstance(rval["t"], Real):
                rval["t"] = [rval["t"]]
            elif not isinstance(rval["t"], Iterable):
                raise TypeError("times should be an iterable of numeric")

            # check iterable or at least numeric
            if not isinstance(rval["vals"], Iterable) and isinstance(rval["vals"], Real):
                rval["vals"] = [rval["vals"]]
            elif not isinstance(rval["vals"], Iterable):
                raise TypeError("values should be an iterable of numeric")

            rval = Timeseries("radius", rval["t"], rval["vals"])
        elif not isinstance(rval, Timeseries):
            raise TypeError(f"data type for radius not understood: '{type(rval)}'")

            # check radius vals are numeric
        if rval.vals.dtype.kind not in ('i', 'f'):
            raise TypeError(f"radius values should be numeric, but got '{rval.vals.dtype}'")

            # check nonnegative and not all zero
        if (rval.vals < 0).any():
            raise ValueError("values for radius cannot be negative")
        elif (rval.vals == 0).all():
            raise ValueError("values for radius are all zero")

        self._radius = rval

    @property
    def timeseries(self):
        return self._timeseries


class PenetrationGroup:
    def __init__(self, group_id: str, points: Iterable[Point], epochs: Iterable = [], **kwargs):
        """A group of points belonging to a single penetration, with optional epoch data.

        Parameters
        ----------
        group_id : str
            A unique ID for the penetration group.
        points : iterable
            A list of PointModels in this group.
        kwargs : Keyword arguments.
            Other timeseries.
        """
        self._id = None
        self._points = []
        self._epochs = []
        self._timeseries = {}

        self.id = group_id
        self.points = points
        self.epochs = epochs

        if "timeseries" in kwargs:
            ts = kwargs["timeseries"]
            if not isinstance(ts, dict):
                raise TypeError(f"timeseries should be a dict, but you gave '{type(ts)}'")

            for key, val in ts.items():
                if not isinstance(val, Timeseries):
                    raise TypeError(f"timeseries with key '{key}' should be a Timeseries, but you gave '{type(val)}'")
                self._timeseries[key] = val

    def to_three_view(self):
        return ThreeView(self)

    def to_two_view(self):
        return TwoView(self)

    @property
    def epochs(self):
        return self._epochs

    @epochs.setter
    def epochs(self, val):
        if not isinstance(val, Iterable):
            raise TypeError("specify a list of epochs")
        if not all(isinstance(e, dict) for e in val) or not all("id" in e and "start time" in e for e in val):
            raise TypeError("each epoch must be a dict with keys 'id' and 'start time'")
        if not all(isinstance(e["start time"], Real) for e in val):
            raise TypeError("each epoch's start time must be a real number")

        for e in val:
            e["id"] = str(e["id"])

        self._epochs = sorted(val, key=lambda e: e["start time"])

    @property
    def timeseries(self):
        return self._timeseries


def point_from_dict(point_dict: dict) -> Point:
    """Construct a PointModel object from a dict.

    Parameters
    ----------
    point_dict : dict
        Must have the following fields:
            id: str
            x: Real
            y: Real
            z: Real
            color: str or dict of times, vals
            radius: Real or dict of times, vals
            alpha: Real or dict of times, vals

    Returns
    -------
    point_model : Point
        PointModel corresponding to the point specified in json_dict.
    """
    required_keys = ("id", "x", "y", "z", "color", "radius", "alpha")
    for key in required_keys:
        if key not in point_dict:
            raise KeyError(f"Missing required key '{key}' in point")

    timeseries = {}
    for key, val in point_dict.items():
        if key in required_keys:
            continue
        if "t" in val and "vals" in val:
            units = val["units"] if "units" in val else ""
            timeseries[key] = Timeseries(label=key, times=val["t"], vals=val["vals"], units=units)

    return Point(point_dict["id"], point_dict["x"], point_dict["y"], point_dict["z"], point_dict["alpha"],
                 point_dict["color"], point_dict["radius"], timeseries=timeseries)


def from_json_file(filename: str) -> Tuple[PenetrationGroup]:
    """Construct a PenetrationGroup or collection of PenetrationGroups from a JSON input file.

    Parameters
    ----------
    filename : str
        Path to a JSON file containing a collection of penetration groups.

    Returns
    -------
    penetration_groups : tuple
        PenetrationGroups parsed from JSON file.
    """
    with open(filename, "r") as fh:
        json_str = fh.read()

    return from_json_str(json_str)


def from_json_str(json_str: str) -> Tuple[PenetrationGroup]:
    """Construct a PenetrationGroup or collection of PenetrationGroups from a JSON input string.

        Parameters
        ----------
        json_str : str
            JSON string containing data for penetration groups.

        Returns
        -------
        penetration_groups : tuple
            PenetrationGroups parsed from JSON string.
        """
    json_data = json.loads(json_str)

    penetration_groups = []
    if "penetration groups" not in json_data:
        # assume single penetration group with no epochs
        if "id" in json_data and isinstance(json_data["id"], str) and "points" in json_data:
            json_data["points"] = list(point_from_dict(p) for p in json_data["points"])
            penetration_groups.append(PenetrationGroup(group_id=json_data["id"], points=json_data["points"]))
        else:
            raise ValueError("Missing key 'penetration groups' in JSON string")
    else:
        if "epochs" in json_data:
            epochs = json_data["epochs"]
        else:
            epochs = []

        for pen_group in json_data["penetration groups"]:
            required_keys = ("id", "points")
            for key in required_keys:
                if key not in pen_group:
                    raise KeyError(f"Missing required key '{key}' in penetration group")

            pen_group["points"] = list(point_from_dict(p) for p in pen_group["points"])

            timeseries = {}
            for key, val in pen_group.items():
                if key in required_keys:
                    continue
                if "t" in val and "vals" in val:
                    units = val["units"] if "units" in val else ""
                    timeseries[key] = Timeseries(label=key, times=val["t"], vals=val["vals"], units=units)

            penetration_groups.append(PenetrationGroup(group_id=pen_group["id"], points=pen_group["points"],
                                                       epochs=epochs, timeseries=timeseries))

    return tuple(penetration_groups)


def to_json_str(pen_groups: Union[PenetrationGroup, Iterable[PenetrationGroup]]) -> str:
    if isinstance(pen_groups, PenetrationGroup):
        pen_groups = (pen_groups,)

    if not isinstance(pen_groups, Iterable) or not all(isinstance(p, PenetrationGroup) for p in pen_groups):
        raise TypeError(f"pen_groups should be iterable of PenetrationGroup, but you gave '{type(pen_groups)}'")

    # determine which penetration groups share epochs
    hash_indices = defaultdict(list)
    for i, pen_group in enumerate(pen_groups):
        ehash = hashlib.sha1()
        for epoch in pen_group.epochs:
            ehash.update(epoch["id"].encode())
            ehash.update(np.float64(epoch["start time"]))
        hash_indices[ehash.hexdigest()].append(i)

    json_strs = []
    for indices in hash_indices.values():
        pg = [pen_groups[i] for i in indices]
        pen_groups_dict = {"epochs": pg[0].epochs, "penetration groups": []}
        for group in pg:
            group_dict = {"id": group.id, "points": []}

            for p in group.points:
                p_dict = {"id": p.id, "x": p.x, "y": p.y, "z": p.z}
                if p.alpha.is_constant:
                    p_dict["alpha"] = p.alpha.get(0)
                else:
                    p_dict["alpha"] = {"t": p.alpha.times.tolist(),
                                       "vals": p.alpha.vals.tolist()}
                if p.color.is_constant:
                    p_dict["color"] = p.color.get(0)
                else:
                    p_dict["color"] = {"t": p.color.times.tolist(),
                                       "vals": p.color.vals.tolist()}

                if p.radius.is_constant:
                    p_dict["radius"] = p.radius.get(0)
                else:
                    p_dict["radius"] = {"t": p.radius.times.tolist(),
                                        "vals": p.radius.vals.tolist()}

                for key, ts in p.timeseries.items():
                    p_dict[key] = {"t": ts.times.tolist(), "vals": ts.vals.tolist(), "units": ts.units}

                group_dict["points"].append(p_dict)

            for key, ts in group.timeseries.items():
                group_dict[key] = {"t": ts.times.tolist(), "vals": ts.vals.tolist()}

            pen_groups_dict["penetration groups"].append(group_dict)

        json_strs.append(json.dumps(pen_groups_dict))

    return tuple(json_strs)