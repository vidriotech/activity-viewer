import os
from os import path as op
import requests
from typing import Iterable
import warnings

from allensdk.api.queries.mouse_connectivity_api import MouseConnectivityApi
from allensdk.core.mouse_connectivity_cache import MouseConnectivityCache
import numpy as np
import pythreejs

from activity_viewer.loaders import OBJParser
from activity_viewer.models import APIEndpoint, PenetrationGroup, Point


class Container:
    AVAILABLE_RESOLUTIONS = (10, 25, 50, 100)
    AP_MAX = 13200
    DV_MAX = 8000
    LR_MAX = 11400

    def __init__(self, api_endpoint: APIEndpoint, resolution: int = 25):
        """Container for slices and compartments.

        Parameters
        ----------
        api_endpoint : APIEndpoint
            An endpoint where compartment structures may be queried.
        resolution : int
            Microns/pixel (one of 10, 25, 50, or 100) for slice view and point compartment querying.
        """
        if api_endpoint is None or not isinstance(api_endpoint, APIEndpoint):
            api_endpoint = APIEndpoint()

        self.api_endpoint = api_endpoint

        self._annotation_colormap = self.api_endpoint.tree.get_colormap()
        self._annotation_colormap[0] = [255, 255, 255]  # originally black
        self._annotation_colormap[997] = [136, 136, 136]  # originally white

        self._penetration_groups = {}
        self._slices = {}
        self._compartments = {}

        # set resolution
        if not isinstance(resolution, int):
            raise TypeError(f"resolution should be an integer, but you gave {type(resolution)}")

        if resolution not in self.AVAILABLE_RESOLUTIONS:
            raise ValueError(f"resolution should be one of {', '.join(map(str, self.AVAILABLE_RESOLUTIONS))}")

        self._resolution = resolution

        mcc = MouseConnectivityCache(resolution=self.resolution, ccf_version=self.ccf_version, cache=True,
                                     manifest_file=op.join(self.cache_dir, "mouse_connectivity_manifest.json"))

        # try to load annotation info from cache
        cache_annotation = op.join(self.construct_cache_base(), f"annotation_{self.resolution}.bin")
        if not op.isfile(cache_annotation):
            av, _ = mcc.get_annotation_volume()
            self._annotation_volume = np.memmap(cache_annotation, dtype=av.dtype, mode="w+", offset=0,
                                                shape=av.shape)
            self._annotation_volume[:, :, :] = av
            self._annotation_volume = np.memmap(cache_annotation, dtype=av.dtype, mode="r", offset=0,
                                                shape=av.shape)
            del av
        else:
            self._annotation_volume = np.memmap(cache_annotation, dtype=np.uint32, mode="r", offset=0,
                                                shape=tuple((np.array([self.AP_MAX, self.DV_MAX,
                                                                       self.LR_MAX]) // self.resolution).tolist()))

        # try to load template info from cache
        cache_template = op.join(self.construct_cache_base(), f"template_{self.resolution}.bin")
        if not op.isfile(cache_template):
            tv, _ = mcc.get_template_volume()
            self._template_volume = np.memmap(cache_template, dtype=tv.dtype, mode="w+", offset=0,
                                              shape=tv.shape)
            self._template_volume[:, :, :] = tv
            self._template_volume = np.memmap(cache_template, dtype=tv.dtype, mode="r", offset=0,
                                              shape=tv.shape)
            del tv
        else:
            self._template_volume = np.memmap(cache_template, dtype=np.uint16, mode="r", offset=0,
                                              shape=tuple((np.array([self.AP_MAX, self.DV_MAX,
                                                                     self.LR_MAX]) // self.resolution).tolist()))

        structure_centers = self.api_endpoint.structure_centers
        brain_areas = self.api_endpoint.tree.get_structures_by_id(structure_centers.structure_id.unique())

        self._brain_areas = {}  # brain area models
        bids = np.array([b["id"] for b in brain_areas])
        if (bids == 997).any():
            idx = np.where(bids == 997)[0][0]
            brain_areas[idx]["rgb_triplet"] = [136, 136, 136]  # default color is too light

        for area_data in brain_areas:
            self._brain_areas[area_data["id"]] = {"name": area_data["name"],
                                                  "acronym": area_data["acronym"],
                                                  "rgb_triplet": area_data["rgb_triplet"],
                                                  "graph_order": area_data["graph_order"]}

    def add_compartment(self, area_id: str, compartment: pythreejs.Object3D):
        """Add brain compartment view model to collection.

        Parameters
        ----------
        area_id : str
            Brain area id.
        compartment : Object3D
            View model for this area of the brain.
        """
        if area_id in self._compartments:
            return
        elif area_id not in self._brain_areas:
            raise ValueError(f"unknown brain area: '{area_id}'")

        self._compartments[area_id] = compartment

    def add_penetration_group(self, penetration_group: PenetrationGroup):
        """Add a penetration group.

        Parameters
        ----------
        penetration_group : PenetrationGroup
        """
        if not isinstance(penetration_group, PenetrationGroup):
            raise TypeError(f"data type for penetration_groups not understood: '{type(penetration_group)}'")

        if penetration_group.id in self._penetration_groups:
            raise ValueError(f"penetration group with id '{penetration_group.id}' already loaded")

        self._penetration_groups[penetration_group.id] = penetration_group

    def construct_cache_base(self, ccf_version=None):
        if ccf_version is None:
            ccf_version = self.ccf_version

        cache_base = op.join(self.cache_dir, *ccf_version.split('/'))
        if not op.isdir(cache_base):
            os.makedirs(cache_base)

        return cache_base

    def download_compartment_obj(self, structure_id: int):
        """Load brain compartment from a geometry file into an Object3D.

        Parameters
        ----------
        structure_id: str
            Unique ID for the mesh.

        Returns
        -------
        compartment: pythreejs.Object3D
            Object3D model of compartment.
        """
        output_file = op.join(self.construct_cache_base(self.ccf_version), f"{structure_id}.obj")
        if not op.isfile(output_file):
            latest_ccf = MouseConnectivityApi.CCF_VERSION_DEFAULT  # no structure meshes for older CCF versions

            if self.ccf_version != latest_ccf:  # warn user of mismatch
                msg = f"You have ccf version '{self.ccf_version}', but structures are only available for "\
                      f"'{latest_ccf}'; you may see a disparity."
                warnings.warn(msg)

            output_file = op.join(self.construct_cache_base(latest_ccf), f"{structure_id}.obj")
            if not op.isfile(output_file):
                try:
                    self.reference_space_api.download_structure_mesh(structure_id, latest_ccf, output_file)
                except requests.exceptions.HTTPError as e:
                    raise ValueError(str(e))

        parser = OBJParser()
        with open(output_file, "r") as fh:
            compartment = parser.parse(fh.read())

        return compartment

    def find_compartment_by_point(self, point: Point):
        i, j, k = (np.array([point.x, point.y, self.LR_MAX - point.z])/self.resolution).astype(np.int)
        return self.annotation_volume[i, j, k]

    def find_pseudocoronal_indices(self, point_group: Iterable[Point]):
        coords = np.zeros((len(point_group), 3))
        for i, point in enumerate(point_group):
            coords[i, :] = [point.x, point.y, point.z]

        coords = coords / self.resolution
        X = np.asmatrix(np.hstack((np.ones((coords.shape[0], 1)), coords[:, 1][:, np.newaxis])))
        y = np.asmatrix(coords[:, 0]).T
        XtX = X.T * X
        Xty = X.T * y
        b = np.linalg.solve(XtX, Xty)
        xhat = np.array(X * b).flatten()

        resid = xhat - y
        mae = np.mean(np.abs(resid))
        mse = np.mean(np.inner(resid, resid))

        # predict x coordinates
        ymax = self._template_volume.shape[1]
        X2 = np.asmatrix(np.hstack((np.ones((ymax, 1)), np.arange(ymax)[:, np.newaxis])))
        return np.array(X2 * b).flatten().astype(np.int)

    def get_annotation_slice(self, penetration_group: PenetrationGroup):
        """

        Parameters
        ----------
        penetration_group
        """
        gid = penetration_group.id

        if gid not in self.slices:  # compute all slices and cache them
            template_slice, annotation_slice, reference_slice = self.get_pseudocoronal_slices(penetration_group)
            self.slices[gid] = (template_slice, annotation_slice, reference_slice)

        return self.slices[gid][1]

    def get_annotation_slice_by_id(self, gid: str):
        """

        Parameters
        ----------
        gid : ID of penetration group.
        """
        return self.get_annotation_slice(self._penetration_groups[gid])

    def get_pseudocoronal_slices(self, penetration_group: PenetrationGroup):
        indices = self.find_pseudocoronal_indices(penetration_group.points)
        template_slice = self._template_volume[indices, np.arange(self._template_volume.shape[1]), :]
        ref_slice = self._annotation_volume[indices, np.arange(self._annotation_volume.shape[1]), :].squeeze()
        ann_slice = np.reshape([self._annotation_colormap[point] for point in ref_slice.flat],
                               list(ref_slice.shape) + [3]).astype(np.uint8)

        return template_slice, ann_slice, ref_slice

    def get_reference_slice(self, penetration_group: PenetrationGroup):
        """

        Parameters
        ----------
        penetration_group
        """
        gid = penetration_group.id

        if gid not in self.slices:  # compute all slices and cache them
            template_slice, annotation_slice, reference_slice = self.get_pseudocoronal_slices(penetration_group)
            self.slices[gid] = (template_slice, annotation_slice, reference_slice)

        return self.slices[gid][2]

    def get_reference_slice_by_id(self, gid: str):
        """

        Parameters
        ----------
        gid : ID of penetration group.
        """
        return self.get_reference_slice(self._penetration_groups[gid])

    def get_template_slice(self, penetration_group: PenetrationGroup):
        """

        Parameters
        ----------
        penetration_group
        """
        gid = penetration_group.id

        if gid not in self.slices:  # compute all slices and cache them
            template_slice, annotation_slice, reference_slice = self.get_pseudocoronal_slices(penetration_group)
            self.slices[gid] = (template_slice, annotation_slice, reference_slice)

        return self.slices[gid][0]

    def get_template_slice_by_id(self, gid):
        """

        Parameters
        ----------
        gid : ID of penetration group.

        Returns
        -------

        """
        return self.get_template_slice(self._penetration_groups[gid])

    @property
    def api_endpoint(self):
        return self._api_endpoint

    @api_endpoint.setter
    def api_endpoint(self, val):
        assert isinstance(val, APIEndpoint)
        self._api_endpoint = val

    @property
    def annotation_colormap(self):
        return self._annotation_colormap

    @property
    def annotation_volume(self):
        return self._annotation_volume

    @property
    def brain_areas(self):
        """Brain area metadata."""
        return self._brain_areas

    @property
    def cache_dir(self):
        return self.api_endpoint.cache_dir

    @property
    def ccf_version(self):
        return self.api_endpoint.ccf_version

    @property
    def compartments(self):
        return self._compartments

    @property
    def penetration_groups(self):
        return self._penetration_groups

    @property
    def reference_space_api(self):
        return self.api_endpoint.reference_space_api

    @property
    def resolution(self):
        return self._resolution

    @property
    def slices(self):
        return self._slices

    @property
    def template_volume(self):
        return self._template_volume
