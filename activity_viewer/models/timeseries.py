from typing import Iterable

import numpy as np


class Timeseries:
    def __init__(self, label: str, times: np.ndarray, vals: np.ndarray, units: str = None):
        self._times = self._vals = self._label = self._units = None

        self.times = times
        self.vals = vals
        self.label = label
        self.units = units if units is not None else "A.U."

    def get(self, t):
        if self.is_constant:
            return self.vals[0]
        elif self.is_valid:
            idx = max(0, min(np.searchsorted(self.times, t, side="left"), self.times.size-1))
            return self._vals[idx]

    @property
    def is_constant(self):
        return self.is_valid and self.times.size == 1

    @property
    def is_valid(self):
        return self.times is not None and self.vals is not None

    @property
    def label(self):
        return self._label

    @label.setter
    def label(self, val):
        self._label = str(val)

    @property
    def times(self):
        return self._times

    @times.setter
    def times(self, tval):
        if not isinstance(tval, np.ndarray) and isinstance(tval, Iterable):
            tval = np.array(tval)
        elif not isinstance(tval, np.ndarray):
            raise TypeError(f"data type for times not understood: '{type(tval)}'")

        if self.vals is not None and tval.size != self.vals.shape[0]:
            raise ValueError(f"vals has count {self.vals.shape[0]} but you are giving times of size {tval.size}")

        # additional constraints
        if tval.dtype.kind not in ('i', 'f'):  # numeric (signed int or float)
            raise TypeError(f"data type for times should be numeric, but you gave '{tval.dtype}'")

        if (np.diff(tval) <= 0).any():  # strictly increasing
            raise ValueError("times should be strictly increasing")

        self._times = tval.flatten()

    @property
    def units(self):
        return self._units

    @units.setter
    def units(self, val):
        self._units = str(val)

    @property
    def vals(self):
        return self._vals

    @vals.setter
    def vals(self, vval):
        if not isinstance(vval, np.ndarray) and isinstance(vval, Iterable):
            vval = np.array(vval)
        elif not isinstance(vval, np.ndarray):
            raise TypeError(f"data type for vals not understood: '{type(vval)}'")

        if self.times is not None and vval.shape[0] != self.times.size:
            raise ValueError(f"times has size {self.times.size} but you are giving vals of size {vval.shape[0]}")

        self._vals = vval
