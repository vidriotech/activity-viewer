import numpy as np
from matplotlib.colors import hex2color, rgb2hex

from pythreejs import (AnimationAction, AnimationClip, AnimationMixer, BufferAttribute, BufferGeometry,
                       ColorKeyframeTrack, Group, ImageTexture, NumberKeyframeTrack, Points, PointsMaterial)

from activity_viewer.constants import DISC_TEXTURE
import activity_viewer.models
from activity_viewer.models import Timeseries


class PointView:
    def __init__(self, penetration_group):
        """

        Parameters
        ----------
        penetration_group : PenetrationGroup
        """
        if not isinstance(penetration_group, activity_viewer.models.PenetrationGroup):
            raise TypeError(f"penetration_group should be a PenetrationGroup, but you gave '{type(penetration_group)}'")

        self._id = penetration_group.id
        self._labels = [p.id for p in penetration_group.points]
        self._x = self._y = self._z = None

        points = penetration_group.points

        # aggregate all t values
        t_a = np.concatenate([p.alpha.times for p in points])
        t_c = np.concatenate([p.color.times for p in points])
        t_r = np.concatenate([p.radius.times for p in points])

        t = np.unique(np.concatenate((t_a, t_c, t_r)))
        if t.size > 1:  # remove -inf if it's there
            t = np.setdiff1d(t, -np.inf)

        v = np.zeros((t.size, len(points), 5))  # rgba(radius)

        for i, ti in enumerate(t):
            for j, p in enumerate(points):
                v[i, j, :] = list(hex2color(p.color.get(ti))) + [p.alpha.get(ti), p.radius.get(ti)]

        self._vals = Timeseries("color_radius", times=t, vals=v)

        # aggregate pointwise timeseries
        self._timeseries = []

        tskeys = set()
        for p in points:
            tskeys.update(list(p.timeseries.keys()))

        for tskey in tskeys:
            col_mask = np.array([tskey in p.timeseries for p in points])

            tst = np.unique(np.concatenate(
                [p.timeseries[tskey].times for i, p in enumerate(points) if col_mask[i]]))
            if tst.size > 1:  # remove -inf if it's there
                tst = np.setdiff1d(tst, -np.inf)

            tsv = np.zeros((tst.size, len(points)))
            tsv[:, ~col_mask] = np.nan
            for i, ti in enumerate(tst):
                for j, p in enumerate(points):
                    if col_mask[j]:
                        jval = p.timeseries[tskey].get(ti)
                        if jval.size > 1:  # multivariate timeseries, skip
                            col_mask[j] = False
                            tsv[:, j] = np.nan
                            continue

                        tsv[i, j] = jval

            if not np.isnan(tsv).all():
                self._timeseries.append(Timeseries(label=tskey, times=tst, vals=tsv))

    def color(self, t):
        return self._vals.get(t)[:, :4]

    def color_radius(self, t):
        cr = self._vals.get(t)
        return cr[:, :4], cr[:, 4]

    def radius(self, t):
        return self._vals.get(t)[:, 4]

    @property
    def _unique_tuples(self):
        """

        Returns
        -------

        """
        ut = set(tuple(v) for v in self._vals.vals.reshape((self.n_points*self.n_timepoints, self._vals.vals.shape[2])))
        return {t: i for i, t in enumerate(ut)}

    @property
    def alphas(self):
        return self._vals.vals[:, :, 3]

    @property
    def hexcolors(self):
        return np.apply_along_axis(rgb2hex, 2, self._vals.vals[:, :, :3])

    @property
    def id(self):
        return self._id

    @property
    def labels(self):
        return self._labels

    @property
    def n_points(self):
        return self._vals.vals.shape[1]

    @property
    def n_timepoints(self):
        return self._vals.times.size

    @property
    def radii(self):
        return self._vals.vals[:, :, 4]

    @property
    def t(self):
        return self._vals.times

    @property
    def timeseries(self):
        return self._timeseries

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y


class ThreeView(PointView):
    """Construct a ThreeJS view object, with animation, from this PenetrationGroup.

    Returns
    -------
    point_view_group : Group
        ThreeJS geometries and materials for these points
    action : AnimationAction
        ThreeJS animation action for this point group, if applicable (else None)
    """
    RAD_SCALE = 50
    def __init__(self, penetration_group):
        """

        Parameters
        ----------
        penetration_group : PenetrationGroup
        """
        super().__init__(penetration_group)

        points = penetration_group.points
        self._x = [p.x for p in points]
        self._y = [p.y for p in points]
        self._z = [p.z for p in points]

        self._action = None
        self._group = Group()
        action_tracks = []

        # try to group as many points as possible
        id_dict = self._unique_tuples
        tuple_ids = np.apply_along_axis(lambda x: id_dict[tuple(x)], 2, self._vals.vals)

        # get number of unique alpha/color/radius values a point takes on as it evolves in time
        # a totally constant point will have 1 in its entry
        unique_timestep_vals = np.apply_along_axis(lambda x: np.unique(x).size, 0, tuple_ids)
        no_evolve = (unique_timestep_vals == 1)  # points which do not evolve over time

        unique_evolutions, inverse_idx, counts = np.unique(tuple_ids, axis=1, return_counts=True, return_inverse=True)
        co_evolve = (counts[inverse_idx] > 1)  # points which evolve in exactly the same way

        hexcolors = self.hexcolors  # precompute to (dramatically) speed up the loops
        group_children = []  # add these all at once

        co_no_evolve = (no_evolve & co_evolve)  # points which do not evolve over time and share all values
        if co_no_evolve.any():
            point_ids = np.where(co_no_evolve)[0]
            col_indices = inverse_idx[co_no_evolve]  # pointers into array of unique columns

            for i in np.unique(col_indices):
                mask = (col_indices == i)
                i_points = point_ids[mask]
                vertices = BufferAttribute(np.array([[points[k].x, points[k].y, points[k].z] for k in i_points],
                                                    dtype=np.float32), normalized=False)
                geometry = BufferGeometry(attributes={"position": vertices})

                # get relevant properties of these points
                u_size = self.radii[0, i_points[0]]
                u_alpha = self.alphas[0, i_points[0]]
                u_color = hexcolors[0, i_points[0]]
                material = PointsMaterial(size=self.__scale(u_size), color=u_color, opacity=u_alpha, alphaTest=0.5,
                                          transparent=True, sizeAttenuation=True, map=ImageTexture(DISC_TEXTURE))
                group_children.append(Points(geometry, material))

        pure_no_evolve = (~co_evolve & no_evolve)
        if pure_no_evolve.any():
            point_ids = np.where(pure_no_evolve)[0]

            for i in point_ids:  # by construction these will already be unique
                vertices = BufferAttribute(np.array([[points[i].x, points[i].y, points[i].z]], dtype=np.float32),
                                           normalized=False)
                geometry = BufferGeometry(attributes={"position": vertices})

                # get relevant properties of this point
                name = f"group{self.id.replace('/', '')}-label{points[i].id.replace('/', '')}"
                u_size = self.radii[0, i]
                u_alpha = self.alphas[0, i]
                u_color = hexcolors[0, i]
                material = PointsMaterial(size=self.__scale(u_size), color=u_color, opacity=u_alpha, alphaTest=0.5,
                                          transparent=True, sizeAttenuation=True, map=ImageTexture(DISC_TEXTURE))
                group_children.append(Points(geometry, material, name=name))

        pure_co_evolve = (co_evolve & ~no_evolve)
        if pure_co_evolve.any():
            point_ids = np.where(pure_co_evolve)[0]
            col_indices = inverse_idx[pure_co_evolve]  # pointers into array of unique columns

            for i in np.unique(col_indices):
                mask = (col_indices == i)
                i_points = point_ids[mask]
                vertices = BufferAttribute(np.array([[points[k].x, points[k].y, points[k].z] for k in i_points],
                                                    dtype=np.float32), normalized=False)
                geometry = BufferGeometry(attributes={"position": vertices})

                # get relevant properties of these points
                u_size = self.radii[0, i_points[0]]
                u_alpha = self.alphas[0, i_points[0]]
                u_color = hexcolors[0, i_points[0]]
                material = PointsMaterial(size=self.__scale(u_size), color=u_color, opacity=u_alpha, alphaTest=0.5,
                                          transparent=True, sizeAttenuation=True, map=ImageTexture(DISC_TEXTURE))

                # create actions for these points
                name = f"group{self.id.replace('/', '')}-copoint{i_points[0]}"
                exemplar = points[i_points[i]]
                if not exemplar.alpha.is_constant:
                    action_tracks.append(
                        NumberKeyframeTrack(name=f"{name}.material.opacity",
                                            times=self.__shift(exemplar.alpha.times.astype(np.float32)),
                                            values=self.__scale(exemplar.alpha.vals.astype(np.float32))))
                if not exemplar.color.is_constant:
                    action_tracks.append(ColorKeyframeTrack(name=f"{name}.material.color",
                                                            times=self.__shift(exemplar.color.times),
                                                            values=self._vals.vals[:, i_points[i], :3]))

                if not exemplar.radius.is_constant:
                    action_tracks.append(
                        NumberKeyframeTrack(name=f"{name}.material.size",
                                            times=self.__shift(exemplar.radius.times.astype(np.float32)),
                                            values=exemplar.radius.vals.astype(np.float32)))

                group_children.append(Points(geometry, material, name=name))

        holdouts = (~co_evolve & ~no_evolve)
        point_ids = np.where(holdouts)[0]
        for i in point_ids:  # by construction these will already be unique
            vertices = BufferAttribute(np.array([[points[i].x, points[i].y, points[i].z]], dtype=np.float32),
                                       normalized=False)
            geometry = BufferGeometry(attributes={"position": vertices})

            # get relevant properties of this point
            u_size = self.radii[0, i]
            u_alpha = self.alphas[0, i]
            u_color = hexcolors[0, i]
            material = PointsMaterial(size=self.__scale(u_size), color=u_color, opacity=u_alpha, alphaTest=0.5,
                                      transparent=True, sizeAttenuation=True, map=ImageTexture(DISC_TEXTURE))

            # create actions for these points
            name = f"group{self.id.replace('/', '')}-label{points[i].id.replace('/', '')}"
            exemplar = points[i]
            if not exemplar.alpha.is_constant:
                action_tracks.append(NumberKeyframeTrack(name=f"{name}.material.opacity",
                                                         times=self.__shift(exemplar.alpha.times.astype(np.float32)),
                                                         values=exemplar.alpha.vals.astype(np.float32)))
            if not exemplar.color.is_constant:
                action_tracks.append(ColorKeyframeTrack(name=f"{name}.material.color",
                                                        times=self.__shift(exemplar.color.times.astype(np.float32)),
                                                        values=self._vals.vals[:, i, :3].astype(np.float32)))

            if not exemplar.radius.is_constant:
                action_tracks.append(
                    NumberKeyframeTrack(name=f"{name}.material.size",
                                        times=self.__shift(exemplar.radius.times.astype(np.float32)),
                                        values=self.__scale(exemplar.radius.vals.astype(np.float32))))

            group_children.append(Points(geometry, material, name=name))

        self._group.add(group_children)

        if len(action_tracks) > 0:
            clip = AnimationClip(tracks=action_tracks)
            self._action = AnimationAction(AnimationMixer(self._group), clip=clip, localRoot=self._group)

    def __scale(self, vals):
        return self.RAD_SCALE * vals

    def __shift(self, times):
        return times - self.t.min()

    @property
    def action(self):
        return self._action

    @property
    def group(self):
        return self._group

    @property
    def z(self):
        return self._z


class TwoView(PointView):
    CORONAL = 0
    SAGITTAL = 1
    HORIZONTAL = 2

    def __init__(self, penetration_group, projection: int = CORONAL):
        """
        Parameters
        ----------
        penetration_group : PenetrationGroup
        projection : int
        """
        super().__init__(penetration_group)

        if not isinstance(projection, int):
            raise TypeError(f"projection should be an int, but you gave '{type(projection)}'")
        elif projection not in (self.CORONAL, self.SAGITTAL, self.HORIZONTAL):
            raise ValueError(f"specify one of TwoView.CORONAL, TwoView.SAGITTAL, TwoView.HORIZONTAL for projection")

        points = penetration_group.points
        # self.x = 11400 - np.array([p.z for p in points])  # correct for reflection
        if projection == self.CORONAL:
            self._x = 11400 - np.array([p.z for p in points])
            self._y = np.array([p.y for p in points])
        elif projection == self.SAGITTAL:
            self._x = np.array([p.x for p in points])
            self._y = np.array([p.y for p in points])
        elif projection == self.HORIZONTAL:
            self._x = np.array([p.z for p in points])
            self._y = np.array([p.x for p in points])

    def best_fit_line(self):
        """Construct two linear fits to the x-y data and take the better of the two.

        Returns
        -------

        """
        def _mae(z, zhat):
            return np.mean(np.abs(z - zhat))

        x, y = self.x, self.y
        xbar, ybar = np.mean(x), np.mean(y)

        # y depends on x
        beta1 = np.dot(x - xbar, y - ybar) / np.dot(x - xbar, x - xbar)
        beta0 = ybar - beta1 * xbar
        yhat = beta0 + x * beta1
        mae_y = _mae(y, yhat)

        # x depends on y
        beta1 = np.dot(x - xbar, y - ybar) / np.dot(y - ybar, y - ybar)
        beta0 = xbar - beta1 * ybar
        xhat = beta0 + beta1 * y
        mae_x = _mae(x, xhat)

        # which is the better fit?
        if mae_y < mae_x:
            return x, yhat
        else:
            return xhat, y
