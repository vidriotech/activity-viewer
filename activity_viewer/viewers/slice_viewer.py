import hashlib
from typing import Iterable, Union

from bokeh.io import push_notebook, show
from bokeh.plotting import figure
from bokeh.layouts import layout, row
from bokeh.models import (BoxSelectTool, BoxZoomTool, ColumnDataSource, CustomJS, CustomJSHover, HoverTool, Label,
                          LabelSet, PanTool, Range1d, Rect, ResetTool, WheelZoomTool)
from bokeh.models.mappers import LinearColorMapper

from IPython.display import display
import ipywidgets
from matplotlib.colors import rgb2hex
import numpy as np

from activity_viewer.models import PenetrationGroup, Container
from activity_viewer.viewers.viewer import BaseViewer, DummyChange


class SliceViewer(BaseViewer):
    ANNOTATION = "Annotation"
    TEMPLATEBW = "Template (B&W)"
    TEMPLATE = "Template (Color)"

    def __init__(self, slice_container: Container,
                 penetration_groups: Union[PenetrationGroup, Iterable[PenetrationGroup]],
                 do_show: bool = True, **kwargs):
        """Container for 2D reference and annotation slice display widget.

        Parameters
        ----------
        slice_container : Container
        penetration_groups : PenetrationGroup or iterable of PenetrationGroup
        do_show : bool
        kwargs : Keyword arguments
        """
        super().__init__(slice_container, penetration_groups, **kwargs)

        show_zoom = "show_zoom" in kwargs and kwargs["show_zoom"] is True

        # ensure that all penetration groups belong on the same plane
        hashes = set()
        for pen_group in self.penetration_groups:
            indices = self.slice_container.find_pseudocoronal_indices(pen_group.points)
            hashes.add(hashlib.new("sha1", indices).hexdigest())

            if len(hashes) > 1:
                raise ValueError("multiple slices don't make sense for this view")

        self._enabled_view = self.ANNOTATION

        # show slice for this group
        ann_slice = self.slice_container.get_annotation_slice(self.penetration_groups[0]).astype(np.uint8)
        ref_slice = self.slice_container.get_reference_slice(self.penetration_groups[0])

        # crop out as much whitespace as possible while keeping aspect ratio close as possible to ideal
        aspect_ratio = self.width / self.height

        iindices = np.where(ref_slice > 0)
        imin, imax = iindices[0].min(), iindices[0].max()
        jmin, jmax = iindices[1].min(), iindices[1].max()

        ifac = int(aspect_ratio < 1)
        jfac = 1 - ifac

        def _delta_k(kay):
            return abs((jmax - jmin + 2 * jfac * kay) / (imax - imin + 2 * ifac * kay) - aspect_ratio)

        k = 0
        delta = _delta_k(0)
        kmax = np.array([jmin + 1, imin + 1, *ref_slice.shape]).min()
        while k < kmax:
            k += 1
            tentative_delta = _delta_k(k)
            if tentative_delta >= delta:
                k -= 1
                break
            else:
                delta = tentative_delta

        imin -= ifac * k
        imax += ifac * (k + 1)
        jmin -= jfac * k
        jmax += jfac * (k + 1)
        ymin, ymax, xmin, xmax = np.array([imin, imax, jmin, jmax]) * self.resolution

        ann_slice = ann_slice[imin:imax, jmin:jmax, :]

        ann_slice_data = np.empty(ann_slice.shape[:2], dtype=np.uint32)
        as_view = ann_slice_data.view(np.uint8).reshape((ann_slice.shape[0], ann_slice.shape[1], 4))
        as_view[:, :, :3] = ann_slice
        as_view[:, :, 3] = 255  # fully opaque

        temp_slice = self.slice_container.get_template_slice(self.penetration_groups[0])[imin:imax, jmin:jmax]

        # display compartments for this group
        sids = np.setdiff1d(np.unique(ref_slice), [0])
        structures = self.slice_container.api_endpoint.tree.get_structures_by_id(sids)
        structures = {sids[i]: s["name"] for i, s in enumerate(structures)}

        names = np.empty(ref_slice.size, dtype=f"<U{max(len(v) for v in structures.values())}")
        ref_slice_flat = ref_slice.flatten()
        for sid, v in structures.items():
            mask = (ref_slice_flat == sid)
            names[mask] = v

        ds = ColumnDataSource(dict(names=[names], stride=[ref_slice.shape[1]]))
        jsc = """let i = Math.floor(special_vars.y/{0});
        let j = Math.floor(special_vars.x/{0});
        let data = ds.attributes.data;
        let names = data.names[special_vars.index];
        let stride = data.stride[special_vars.index];
        let name = names[stride*i + j];
        if (name.length > 0) return name;
        """.format(self.resolution)
        custom = CustomJSHover(code=jsc, args=dict(ds=ds))

        self._slice_fig = figure(plot_width=self._width // (2 - (show_zoom is False)),
                                 plot_height=self.height // (2 - (show_zoom is False)),
                                 x_range=Range1d(start=xmin, end=xmax, bounds=(xmin, xmax)),
                                 y_range=Range1d(start=ymax, end=ymin, bounds=(ymin, ymax)),
                                 tools=[BoxSelectTool(), BoxZoomTool(), PanTool(), ResetTool(), WheelZoomTool()],
                                 toolbar_location="left")
        im = self._slice_fig.image_rgba([np.flipud(ann_slice_data)], x=[xmin], y=[ymax], dw=[xmax - xmin],
                                        dh=[ymax - ymin])

        self._temp_image = self._slice_fig.image([np.flipud(temp_slice)], x=[xmin], y=[ymax], dw=[xmax - xmin],
                                                 dh=[ymax - ymin], palette="Viridis256", visible=False)

        # add hover tools
        ht_im = HoverTool(renderers=[im], tooltips=[("Region", "@y{custom}")], formatters=dict(y=custom))
        self._slice_fig.add_tools(ht_im)  # puts unit ID above compartment

        if show_zoom:
            # set up a second "minimap" figure showing where we're zoomed in
            self._minimap_fig = figure(plot_width=self._width // 2, plot_height=self.height // 2,
                                       x_range=Range1d(start=xmin, end=xmax, bounds=(xmin, xmax)),
                                       y_range=Range1d(start=ymax, end=ymin, bounds=(ymin, ymax)),
                                       tools=[], toolbar_location=None, visible=True)
            self._minimap_fig.image_rgba([np.flipud(ann_slice_data)], x=[xmin], y=[ymax], dw=[xmax - xmin],
                                         dh=[ymax - ymin])
            self._temp_image_mm = self._minimap_fig.image([np.flipud(temp_slice)], x=[xmin], y=[ymax], dw=[xmax - xmin],
                                                          dh=[ymax - ymin], palette="Viridis256", visible=False)

            # add a rectangle glyph around the region zoomed into in the second figure
            range_data_source = ColumnDataSource({"x": [], "y": [], "width": [], "height": []})
            range_callback_src = """
                        let data = source.data;
                        let start = cb_obj.start;
                        let end = cb_obj.end;
                        data["{0}"] = [start + (end - start) / 2];
                        data["{1}"] = [end - start];
                        source.change.emit();
                    """
            self._slice_fig.x_range.callback = CustomJS(code=range_callback_src.format("x", "width"),
                                                        args=dict(source=range_data_source))
            self._slice_fig.y_range.callback = CustomJS(code=range_callback_src.format("y", "height"),
                                                        args=dict(source=range_data_source))
            rect = Rect(x="x", y="y", width="width", height="height", fill_alpha=0.1, line_color="black",
                        line_dash="dashed")
            self._minimap_fig.add_glyph(range_data_source, rect)
            self._layout = layout([row(self._slice_fig, self._minimap_fig)])
        else:
            self._minimap_fig = self._temp_image_mm = None
            self._layout = layout([row(self._slice_fig)])

        # display tracks and points
        self._views = [pg.to_two_view() for pg in self.penetration_groups]
        self._scatter_source = []
        for view in self._views:
            x_track, y_track = view.best_fit_line()

            self._slice_fig.line(x_track, y_track, color="black", line_width=2)  # show track

            # show activity for this group
            scatter_source = ColumnDataSource(dict(id=[], x=[], y=[], radius=[], color=[], alpha=[]))
            pts = self._slice_fig.circle("x", "y", radius="radius", color="color", alpha="alpha", source=scatter_source,
                                         name=f"{view.id}_scatter")
            ht_pts = HoverTool(renderers=[pts], tooltips=[("Unit", "@id")])
            self._slice_fig.add_tools(ht_pts)

            if self._minimap_fig is not None:
                self._minimap_fig.circle("x", "y", radius="radius", color="color", alpha="alpha", source=scatter_source)

            self._scatter_source.append(scatter_source)

        # plot timeseries
        self._timeseries = {}
        for view in self._views:
            self._timeseries[view.id] = view.timeseries

        if any(v for v in self._timeseries.values()):
            # select the first timeseries we come across (for now)
            self._active_timeseries = [k for k, v in self._timeseries.items() if len(v) > 0][0]
            view_idx = self._reverse_pg_map[self._active_timeseries]
            view = self._views[view_idx]

            ts = self._timeseries[self._active_timeseries][0]
            self._timeseries_fig = figure(plot_width=self.width, plot_height=150, title=ts.label,
                                          x_range=(ts.times[0], ts.times[-1]), tools=[], toolbar_location=None,
                                          y_range=(ts.vals.min(), ts.vals.max()))
            self._timeseries_source = ColumnDataSource(
                dict(times=ts.vals.shape[1] * [ts.times],
                     vals=[ts.vals[:, j] for j in range(ts.vals.shape[1])],
                     labels=view.labels,
                     colors=ts.vals.shape[1] * ["#888888"],
                     widths=ts.vals.shape[1] * [1],
                     alphas=ts.vals.shape[1] * [0.3])
            )

            self._timeseries_fig.multi_line(xs="times", ys="vals", color="colors", line_width="widths",
                                            line_alpha="alphas", source=self._timeseries_source)
            self._timeseries_fig.yaxis.visible = False
            jsc = """let data = ts.attributes.data;
                     let indices = cb_data.index["1d"].indices;
                     for (let i = 0; i < data.alphas.length; i++) {
                         if (indices.includes(i)) {
                             data.alphas[i] = 1;
                             data.widths[i] = 3;
                             data.colors[i] = "#FFA500"
                         } else {
                             data.alphas[i] = 0.1 + (indices.length == 0)*0.2;
                             data.widths[i] = 1;
                             data.colors[i] = "#888888";
                         }
                     }

                     ts.change.emit()
                     """
            callback = CustomJS(code=jsc, args=dict(ts=self._timeseries_source))
            glyphs = self._slice_fig.select_one(f"{view.id}_scatter")
            ht_cross = HoverTool(renderers=[glyphs], tooltips=None, callback=callback)
            self._slice_fig.add_tools(ht_cross)

            # scale_fig = figure(plot_width=50, plot_height=150, x_range=([0, 1]), tools=[], toolbar_location=None,
            #                    y_range=self._timeseries_fig.y_range, y_axis_location="right", border_fill_color=None)
            # scale_fig.xgrid[0].grid_line_color = None
            # scale_fig.ygrid[0].grid_line_color = None
            # scale_fig.xaxis.visible = False

            # show a scale bar
            ts_scale = 10**np.floor(np.log10(ts.vals.max() - ts.vals.min()))
            sb_center = min(ts.vals.min() + 0.8*(ts.vals.max() - ts.vals.min()), ts.vals.max() - ts_scale)
            sb_x = 2*[np.percentile(ts.times, 80)]
            sb_y = [sb_center - ts_scale, sb_center + ts_scale]
            self._timeseries_fig.line(sb_x, sb_y, color="black", line_width=3)

            citation = Label(x=sb_x[0], x_offset=5, y=sb_center, y_offset=-7, x_units="data", y_units="data",
                             text=f"{2*ts_scale} {ts.units}", render_mode="canvas", text_font_size="10pt")
            self._timeseries_fig.add_layout(citation)

            self._layout.children.append(row([self._timeseries_fig]))
        else:
            self._active_timeseries = self._timeseries_source = self._timeseries_fig = None

        # visualization tweaks
        self._size_scale = 1.
        self._size_scale_slider = ipywidgets.FloatSlider(value=self._size_scale, min=0.1, max=5.05, step=0.1,
                                                         description="Radius scale", disabled=False,
                                                         readout_format=".2f", continuous_update=False,
                                                         orientation="horizontal", readout=True)
        self._size_scale_slider.observe(self._on_change_size_scale, names="value")

        # animation
        self._t = None
        self._t_value_label = ipywidgets.Label(value="0")
        self._frame_rate_slider = ipywidgets.IntSlider(value=60, min=1, max=120, step=1, description="Frames/sec",
                                                       disabled=True, continuous_update=False, orientation="horizontal",
                                                       readout=True)
        self._play_button = ipywidgets.Play(value=0, min=0, max=1., step=1, description="Press play", disabled=True)
        self._play_button.interval = 17  # ~ 60fps
        self._play_slider = ipywidgets.IntSlider(value=0, min=0, max=1, step=1, disabled=True, readout=False)
        self._link = ipywidgets.link((self._play_slider, "value"), (self._play_button, "value"))

        if any(view.t.size > 1 for view in self._views):
            t = np.unique(np.concatenate([view.t for view in self._views]))
            self._dt = np.diff(t).min() / 4
            self._t = np.arange(t.min(), t.max() + self._dt / 2, self._dt)

            self._play_button.observe(self._animate, names="value")
            self._frame_rate_slider.observe(self._on_change_frame_rate, names="value")
            self._play_slider.disabled = self._play_button.disabled = self._frame_rate_slider.disabled = False

            self._play_slider.min = self._play_button.min = 0
            self._play_slider.max = self._play_button.max = self._t.size - 1  # right-closed interval
            self._play_slider.step = self._play_button.step = 1

            # show epoch slider
            epochs = self.penetration_groups[0].epochs
            if len(epochs) > 0:
                # glyphs for each epoch (alternate light grey w/ dark grey)
                offsets = [e["start time"] for e in epochs]
                dw = [epochs[i + 1]["start time"] - epochs[i]["start time"]
                      for i in range(len(epochs) - 1)] + [self._t[-1] - epochs[-1]["start time"]]
                egs = {"image": [np.array([[183 - 94 * (i % 2), 183 - 94 * (i % 2), 183 - 94 * (i % 2), 255]],
                                          dtype=np.uint8).view(np.uint32) for i in range(len(epochs))],
                       "x": offsets,
                       "dw": dw
                       }

                # also create labels
                epoch_labels = LabelSet(x="x", y="y", text="names", y_offset=2, text_color="white",
                                        source=ColumnDataSource(dict(
                                            x=[offsets[i] + dw[i] / 4 for i in range(len(epochs))],
                                            y=[0] * len(epochs),
                                            names=[e["id"] for e in epochs])
                                        ))
            else:
                egs = {"image": [], "x": [], "dw": []}
                epoch_labels = None

            egs["image"] += [np.array([[4278190335]], dtype=np.uint32)]
            egs["x"] += [self._t[0]]
            egs["dw"] += [0]

            self._epoch_source = ColumnDataSource(
                dict(
                    image=egs["image"], x=egs["x"], y=[0] * (1 + len(epochs)),
                    dw=egs["dw"], dh=[5] * len(epochs) + [0.5]
                )
            )

            self._epoch_slider_fig = figure(plot_width=self.width, plot_height=50,
                                            x_range=(self._t[0], self._t[-1]), y_range=(0, 5),
                                            tools=[], toolbar_location=None)

            self._epoch_slider_fig.yaxis.visible = False
            self._epoch_slider_fig.xgrid.visible = self._epoch_slider_fig.ygrid.visible = False
            self._epoch_slider_fig.image_rgba(image="image", x="x", y="y", dw="dw", dh="dh", source=self._epoch_source)
            if epoch_labels is not None:
                self._epoch_slider_fig.add_layout(epoch_labels)

            if self._timeseries_fig is not None:
                self._timeseries_fig.xaxis.visible = False

            self._layout.children.append(row([self._epoch_slider_fig]))

        # select list of slice views
        self._slice_view = ipywidgets.ToggleButtons(
            options=[self.ANNOTATION, self.TEMPLATEBW, self.TEMPLATE], disabled=False,
            tooltips=["Display slice with annotations", "Display template slice in black and white",
                      "Display template slice in color"],
        )
        self._slice_view.observe(self._on_change_slice_view, names="value")

        # workspace
        self._workspace = ipywidgets.VBox([
            ipywidgets.HBox([
                ipywidgets.Label("Slice view style"), self._slice_view
            ]),
            ipywidgets.HBox([self._play_button, self._t_value_label, self._play_slider]),
            ipywidgets.VBox([self._size_scale_slider, self._frame_rate_slider])
        ], layout=ipywidgets.Layout(display="flex", flex_flow="column"))

        if do_show:
            self._handle = show(self._layout, notebook_handle=True)
            display(self._workspace)

            if self._t is not None:
                self._animate(DummyChange(0))

    def _animate(self, change):
        frame = change.new

        t = self._t[frame]
        self._t_value_label.value = f"{t:0.2f}"

        for i, view in enumerate(self._views):
            n_points = view.x.size
            color, radius = view.color_radius(t)
            new_scatter_data = {"color": [rgb2hex(c) for c in color[:, :3]], "alpha": color[:, -1], "id": view.labels,
                                "radius": radius * self._size_scale, "x": view.x, "y": view.y}
            self._scatter_source[i].stream(new_scatter_data, rollover=n_points)

        new_epoch_data = {"image": self._epoch_source.data["image"],
                          "x": self._epoch_source.data["x"], "y": self._epoch_source.data["y"],
                          "dw": self._epoch_source.data["dw"][:-1] + [frame * self._dt],
                          "dh": self._epoch_source.data["dh"]}
        self._epoch_source.stream(new_epoch_data, rollover=1 + len(self.penetration_groups[0].epochs))

        push_notebook(handle=self._handle)

    def _on_change_frame_rate(self, change):
        self._play_button.interval = int(1000/change.new)

    def _on_change_size_scale(self, change):
        self._size_scale = change.new
        if not self._play_button._playing:
            self._animate(DummyChange(self._play_button.value))

    def _on_change_slice_view(self, change):
        self._enabled_view = change.new
        if self._enabled_view == self.ANNOTATION:
            self._temp_image.visible = False
            if self._temp_image_mm is not None:
                self._temp_image_mm.visible = False
        elif self._enabled_view == self.TEMPLATE:
            self._temp_image.visible = True
            self._temp_image.glyph.color_mapper = LinearColorMapper("Viridis256")

            if self._temp_image_mm is not None:
                self._temp_image_mm.visible = True
                self._temp_image_mm.glyph.color_mapper = LinearColorMapper("Viridis256")
        else:
            self._temp_image.visible = True
            self._temp_image.glyph.color_mapper = LinearColorMapper("Greys256")

            if self._temp_image_mm is not None:
                self._temp_image_mm.visible = True
                self._temp_image_mm.glyph.color_mapper = LinearColorMapper("Greys256")

        push_notebook(handle=self._handle)

    @property
    def api_endpoint(self):
        return self.slice_container.api_endpoint

    @property
    def resolution(self):
        return self._container.resolution

    @property
    def slice_container(self):
        return self._container
