from collections import defaultdict, OrderedDict
import hashlib
from typing import Iterable, Union

from bokeh.io import push_notebook, show
from bokeh.layouts import layout, row
from bokeh.models import (BoxSelectTool, BoxZoomTool, ColumnDataSource, CustomJSHover, HoverTool, LabelSet, PanTool,
                          Range1d, ResetTool, WheelZoomTool)
from bokeh.plotting import figure

from IPython.display import display
import ipywidgets
from matplotlib.colors import rgb2hex
import numpy as np
from scipy.spatial.distance import pdist, squareform

from activity_viewer.models import PenetrationGroup, Container
from activity_viewer.viewers.viewer import BaseViewer, DummyChange


class PenetrationViewer(BaseViewer):
    def __init__(self, slice_container: Container,
                 penetration_groups: Union[PenetrationGroup, Iterable[PenetrationGroup]],
                 do_show: bool = True,
                 **kwargs):
        """Display a compact representation of a penetration.

        Parameters
        ----------
        slice_container
        penetration_groups
        kwargs
        """
        super().__init__(slice_container, penetration_groups, **kwargs)

        # get distinct planes for penetration groups
        hash_indices = defaultdict(list)
        for i, pen_group in enumerate(self.penetration_groups):
            indices = self.slice_container.find_pseudocoronal_indices(pen_group.points)
            h = hashlib.new("sha1", indices).hexdigest()
            hash_indices[h].append(i)

        hashes = sorted(list(hash_indices.keys()), key=lambda h: len(hash_indices[h]), reverse=True)

        slice_row = []
        activity_row = []
        self._x = []
        self._views = []
        self._scatter_source = []
        for h in hashes:
            pen_groups = [self.penetration_groups[i] for i in hash_indices[h]]

            # show slice for this group or groups
            ann_slice = self.slice_container.get_annotation_slice(pen_groups[0]).astype(np.uint8)
            ref_slice = self.slice_container.get_reference_slice(pen_groups[0])

            # crop out as much whitespace as possible while keeping aspect ratio close as possible to ideal
            h_prop = len(pen_groups) / len(self.penetration_groups)
            fig_width = int(h_prop * self.width)
            fig_height = int(0.75 * self.height)

            aspect_ratio = fig_width/fig_height

            iindices = np.where(ref_slice > 0)
            imin, imax = iindices[0].min(), iindices[0].max()
            jmin, jmax = iindices[1].min(), iindices[1].max()

            ifac = int(aspect_ratio < 1)
            jfac = 1 - ifac

            def _delta_k(kay):
                return abs((jmax - jmin + 2*jfac*kay)/(imax - imin + 2*ifac*kay) - aspect_ratio)

            k = 0
            delta = _delta_k(0)
            kmax = np.array([jmin + 1, imin + 1, *ref_slice.shape]).min()
            while k < kmax:
                k += 1
                tentative_delta = _delta_k(k)
                if tentative_delta >= delta:
                    k -= 1
                    break
                else:
                    delta = tentative_delta

            imin = max(0, imin - ifac * k)
            imax = min(ref_slice.shape[0] - 1, imax + ifac * (k + 1))
            jmin = max(0, jmin - jfac * k)
            jmax = min(ref_slice.shape[1] - 1, jmax + jfac * (k + 1))
            ymin, ymax, xmin, xmax = np.array([imin, imax, jmin, jmax]) * self.resolution

            ann_slice = ann_slice[imin:imax, jmin:jmax, :]

            ann_slice_data = np.empty(ann_slice.shape[:2], dtype=np.uint32)
            as_view = ann_slice_data.view(np.uint8).reshape((ann_slice.shape[0], ann_slice.shape[1], 4))
            as_view[:, :, :3] = ann_slice
            as_view[:, :, 3] = 255

            fig = figure(plot_width=int(h_prop * self.width), plot_height=int(0.75 * self.height),
                         x_range=Range1d(start=xmin, end=xmax, bounds=(xmin, xmax)),
                         y_range=Range1d(start=ymax, end=ymin, bounds=(ymin, ymax)),
                         tools=[], toolbar_location=None)
            fig.image_rgba([np.flipud(ann_slice_data)], x=[xmin], y=[ymax], dw=[xmax-xmin], dh=[ymax-ymin])
            slice_row.append(fig)

            views = [pg.to_two_view() for pg in pen_groups]
            for view in views:
                x_track, y_track = view.best_fit_line()
                fig.line(x_track, y_track, color="black", line_width=2)

                # show activity for this group
                r = view.radii.max(axis=1)[:, np.newaxis]  # maximum radius for each point at any time
                yy = squareform(pdist(view.y[:, np.newaxis], "sqeuclidean"))
                rr = (r + r.T) ** 2

                scatter_x = np.zeros(view.x.size)
                for j in range(scatter_x.size):
                    not_j = np.setdiff1d(range(scatter_x.size), j)
                    rrj = rr[j, not_j]
                    yyj = yy[j, not_j]
                    rhs = rrj - yyj
                    i = np.argmax(rhs)
                    rhs = rhs[i]
                    if rhs < 0:  # y's are sufficiently far apart, leave at 0
                        continue

                    scatter_x[j] = scatter_x[i] + (np.random.choice((-1, 1)) * np.sqrt(rhs)) + np.random.rand() * np.std(scatter_x)

                self._x.append(scatter_x)
                xmin, xmax = scatter_x.min() - np.std(scatter_x), scatter_x.max() + np.std(scatter_x)
                ymin = view.y.min() - view.radii[np.argmin(view.y), :].max()
                ymax = view.y.max() + view.radii[np.argmax(view.y), :].max()

                scatter_source = ColumnDataSource(dict(id=[], x=[], y=[], radius=[], color=[], alpha=[]))
                scatter_fig = figure(plot_width=int(0.8/len(self.penetration_groups) * self.width),
                                     plot_height=int(1.75 * self.height),
                                     x_range=Range1d(start=xmin, end=xmax, bounds=(xmin, xmax)),
                                     y_range=Range1d(start=ymin, end=ymax, bounds=(ymin, ymax)),
                                     tools=[BoxSelectTool(), BoxZoomTool(), HoverTool(tooltips=[("Unit", "@id")]),
                                            PanTool(), ResetTool(), WheelZoomTool()], toolbar_location="above")
                scatter_fig.xaxis.visible = scatter_fig.yaxis.visible = False
                scatter_fig.circle("x", "y", radius="radius", color="color", alpha="alpha", source=scatter_source)

                self._scatter_source.append(scatter_source)

                # get y values such that, when truncated, they will be unique (and x values corresponding to these)
                x, y = view.x, view.y
                _, argu = np.unique(y.astype(np.int), return_index=True)
                x, y = x[argu], y[argu]

                # fit a line with y as independent variable
                xbar, ybar = np.mean(x), np.mean(y)
                beta1 = np.dot(x - xbar, y - ybar) / np.dot(y - ybar, y - ybar)
                beta0 = xbar - beta1 * ybar

                # take y range, ymin to ymax, and get fitted x values
                y = self.resolution * np.arange(np.floor(y.min() / self.resolution), np.ceil(y.max() / self.resolution))
                x = beta0 + beta1 * y

                # get pixel values in reference slice
                yhat = (y / self.resolution).astype(np.int)
                xhat = (x / self.resolution).astype(np.int)
                cdata = ref_slice[yhat, xhat]

                compartment_data = np.empty((cdata.size, 1), dtype=np.uint32)
                cd_view = compartment_data.view(dtype=np.uint8).reshape((cdata.size, 1, 4))
                cd_view[:, :, :3] = np.reshape([self.annotation_colormap[c] for c in cdata],
                                               (cdata.size, 1, 3)).astype(np.uint8)
                cd_view[:, :, 3] = 255  # alpha = 1

                # custom tooltip displaying compartment acronym
                jsc = """let resolution = {0};
                        let ymin = {1};
                        let cdata = {2};
                        let smap_acro = {3};
                        let i = Math.floor(special_vars.y/resolution) - ymin;
                        return smap_acro["_" + cdata[i]];
                        """.format(self.resolution, yhat.min(), cdata.tolist(),
                                   {f"_{c}": self.api_endpoint.tree.get_structures_by_id([c])[0]["acronym"] for c in
                                    np.unique(cdata)})
                custom = CustomJSHover(code=jsc)

                compartment_fig = figure(plot_width=int(0.2/len(self.penetration_groups) * self.width),
                                         plot_height=int(1.75 * self.height), x_range=Range1d(bounds=(0, 1)),
                                         y_range=scatter_fig.y_range, toolbar_location=None,
                                         tools=[PanTool(), ResetTool(), WheelZoomTool(),
                                                HoverTool(tooltips=[("", "@y{custom}")], formatters=dict(y=custom))])
                compartment_fig.xaxis.visible = False
                compartment_fig.image_rgba([compartment_data], x=[0], y=[view.y.min()], dw=[1],
                                           dh=[view.y.max() - view.y.min()])

                activity_row += [compartment_fig, scatter_fig]

            self._views += views

        self._layout = layout([row(slice_row), row(activity_row)])

        # visualization tweaks
        self._size_scale = 1.
        self._size_scale_slider = ipywidgets.FloatSlider(value=self._size_scale, min=0.1, max=5.05, step=0.1,
                                                         description="Radius scale", disabled=False,
                                                         readout_format=".2f", continuous_update=False,
                                                         orientation="horizontal", readout=True)
        self._size_scale_slider.observe(self._on_change_size_scale, names="value")

        # animation
        self._t = None
        self._t_value_label = ipywidgets.Label(value="0")
        self._frame_rate_slider = ipywidgets.IntSlider(value=60, min=1, max=120, step=1, description="Frames/sec",
                                                       disabled=True, continuous_update=False, orientation="horizontal",
                                                       readout=True)
        self._play_button = ipywidgets.Play(value=0, min=0, max=1., step=1, description="Press play", disabled=True)
        self._play_button.interval = 17  # ~ 60fps
        self._play_slider = ipywidgets.IntSlider(value=0, min=0, max=1, step=1, disabled=True, readout=False)
        self._link = ipywidgets.link((self._play_slider, "value"), (self._play_button, "value"))

        if any(view.t.size > 1 for view in self._views):
            t = np.unique(np.concatenate([view.t for view in self._views]))
            self._dt = np.diff(t).min() / 4
            self._t = np.arange(t.min(), t.max() + self._dt / 2, self._dt)

            self._play_button.observe(self._animate, names="value")
            self._frame_rate_slider.observe(self._on_change_frame_rate, names="value")
            self._play_slider.disabled = self._play_button.disabled = self._frame_rate_slider.disabled = False

            self._play_slider.min = self._play_button.min = 0
            self._play_slider.max = self._play_button.max = self._t.size - 1  # right-closed interval
            self._play_slider.step = self._play_button.step = 1

            # show epoch slider
            epochs = self.penetration_groups[0].epochs
            if len(epochs) > 0:
                # glyphs for each epoch (alternate light grey w/ dark grey)
                offsets = [e["start time"] for e in epochs]
                dw = [epochs[i + 1]["start time"] - epochs[i]["start time"]
                      for i in range(len(epochs) - 1)] + [self._t[-1] - epochs[-1]["start time"]]
                egs = {"image": [np.array([[183 - 94 * (i % 2), 183 - 94 * (i % 2), 183 - 94 * (i % 2), 255]],
                                          dtype=np.uint8).view(np.uint32) for i in range(len(epochs))],
                       "x": offsets,
                       "dw": dw
                       }

                # also create labels
                epoch_labels = LabelSet(x="x", y="y", text="names", y_offset=2, text_color="white",
                                        source=ColumnDataSource(
                                            {"x": [offsets[i] + dw[i] / 4 for i in range(len(epochs))],
                                             "y": [0] * len(epochs),
                                             "names": [e["id"] for e in epochs]
                                             }))
            else:
                egs = {"image": [], "x": [], "dw": []}
                epoch_labels = None

            egs["image"] += [np.array([[4278190335]], dtype=np.uint32)]
            egs["x"] += [self._t[0]]
            egs["dw"] += [0]

            self._epoch_source = ColumnDataSource({"image": egs["image"], "x": egs["x"], "y": [0] * (1 + len(epochs)),
                                                   "dw": egs["dw"], "dh": [5] * len(epochs) + [0.5]})

            self._epoch_slider_fig = figure(plot_width=self.width,
                                            plot_height=50,
                                            x_range=(self._t[0], self._t[-1]), y_range=(0, 5),
                                            tools=[], toolbar_location=None)
            self._epoch_slider_fig.yaxis.visible = False
            self._epoch_slider_fig.xgrid.visible = self._epoch_slider_fig.ygrid.visible = False
            self._epoch_slider_fig.image_rgba(image="image", x="x", y="y", dw="dw", dh="dh", source=self._epoch_source)
            if epoch_labels is not None:
                self._epoch_slider_fig.add_layout(epoch_labels)

            self._layout.children = self._layout.children + [row([self._epoch_slider_fig])]

        # container for all widgets
        self._workspace = ipywidgets.VBox([
            ipywidgets.HBox([self._play_button, self._t_value_label, self._play_slider]),
            ipywidgets.VBox([self._size_scale_slider, self._frame_rate_slider])
        ], layout=ipywidgets.Layout(display="flex", flex_flow="column"))

        if do_show:
            self._handle = show(self._layout, notebook_handle=True)
            display(self._workspace)

            if self._t is not None:
                self._animate(DummyChange(0))

    def _animate(self, change):
        frame = change.new

        t = self._t[frame]
        self._t_value_label.value = f"{t:0.2f}"

        for i, view in enumerate(self._views):
            n_points = view.x.size
            color, radius = view.color_radius(t)

            new_scatter_data = {"color": [rgb2hex(c) for c in color[:, :3]], "alpha": color[:, -1], "id": view.labels,
                                "radius": radius * self._size_scale, "x": self._x[i], "y": view.y}
            self._scatter_source[i].stream(new_scatter_data, rollover=n_points)

        new_epoch_data = {"image": self._epoch_source.data["image"],
                          "x": self._epoch_source.data["x"], "y": self._epoch_source.data["y"],
                          "dw": self._epoch_source.data["dw"][:-1] + [frame * self._dt],
                          "dh": self._epoch_source.data["dh"]}
        self._epoch_source.stream(new_epoch_data, rollover=1 + len(self.penetration_groups[0].epochs))

        push_notebook(handle=self._handle)

    def _on_change_frame_rate(self, change):
        self._play_button.interval = int(1000/change.new)

    def _on_change_size_scale(self, change):
        self._size_scale = change.new
        if not self._play_button._playing:
            self._animate(DummyChange(self._play_button.value))

    @property
    def api_endpoint(self):
        return self.slice_container.api_endpoint

    @property
    def annotation_colormap(self):
        return self.slice_container.annotation_colormap

    @property
    def resolution(self):
        return self._container.resolution

    @property
    def slice_container(self):
        return self._container
