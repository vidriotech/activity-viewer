from numbers import Real
from typing import Iterable, Union

from bokeh.io import push_notebook, show
from IPython.display import display
from numpy import unique

from activity_viewer.models import PenetrationGroup
from activity_viewer.models.container import Container


class BaseViewer:
    _default_height = 600
    _aspect_ratio = 57 / 40

    def __init__(self, container: Container,
                 penetration_groups: Union[PenetrationGroup, Iterable[PenetrationGroup]], **kwargs):
        """Container for display widgets.
        """

        self._handle = None
        self._height = self._width = 0
        self._penetration_groups = ()
        self._handle = self._layout = self._workspace = self._container = None

        if "height" not in kwargs and "width" in kwargs:
            kwargs["height"] = int(kwargs["width"]/self._aspect_ratio)
        elif "height" not in kwargs:
            kwargs["height"] = self._default_height
        if "width" not in kwargs:
            kwargs["width"] = int(self._aspect_ratio * kwargs["height"])

        self.height = kwargs["height"]
        self.width = kwargs["width"]

        self.penetration_groups = penetration_groups
        if self.n_penetrations == 0:
            raise ValueError("at least one penetration group required")

        self.container = container

    def hide(self):
        if self._handle is not None:
            self._layout.visible = False
            push_notebook(handle=self._handle)
            self._workspace.layout.visibility = "hidden"

    def show(self):
        if self._handle is None:  # show for the first time
            self._handle = show(self._layout, notebook_handle=True)
            display(self._workspace)
        else:
            self._layout.visible = True
            push_notebook(handle=self._handle)
            self._workspace.layout.visibility = "visible"

    @property
    def container(self):
        return self._container

    @container.setter
    def container(self, val):
        assert isinstance(val, Container)
        self._container = val

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, val):
        assert isinstance(val, Real) and val > 0
        self._height = int(val)

    @property
    def n_penetrations(self):
        return len(self._penetration_groups)

    @property
    def penetration_groups(self):
        return self._penetration_groups

    @penetration_groups.setter
    def penetration_groups(self, val):
        if isinstance(val, PenetrationGroup):
            val = [val]
        if not isinstance(val, Iterable):
            raise TypeError(f"data type for penetration_groups not understood: '{type(val)}'")
        if not all(isinstance(v, PenetrationGroup) for v in val):
            raise TypeError(f"not all penetration groups supplied were instance of PenetrationGroup")

        if unique([v.id for v in val]).size < len(val):
            raise ValueError(f"penetration groups have duplicated ids")

        self._penetration_groups = tuple(val)
        self._reverse_pg_map = {v.id: i for i, v in enumerate(self._penetration_groups)}

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, val):
        assert isinstance(val, Real) and val > 0
        self._width = int(val)


class DummyChange:
    def __init__(self, value):
        self.new = value