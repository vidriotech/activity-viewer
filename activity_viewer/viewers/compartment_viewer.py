from collections import defaultdict
from numbers import Integral
import re
from typing import Iterable, Union

from bokeh.io import push_notebook, show
from bokeh.plotting import figure
from bokeh.layouts import layout, row
from bokeh.models import ColumnDataSource, LabelSet
from pythreejs import (AmbientLight, AnimationAction, AnimationClip, AnimationMixer,
                       DirectionalLight, Group, OrbitControls, PerspectiveCamera, Renderer, Scene, ShaderMaterial)

from IPython.display import display
import ipywidgets
from matplotlib.colors import rgb2hex
import numpy as np

from activity_viewer.constants import CAMERA_START_POSITION, CAMERA_START_QUATERNION, CAMERA_START_ROTATION
from activity_viewer.models import PenetrationGroup, Container
from activity_viewer.shaders import COMPARTMENT_VERTEX_SHADER, COMPARTMENT_FRAGMENT_SHADER
from activity_viewer.viewers.viewer import BaseViewer, DummyChange


def _shader_factory(color: str) -> ShaderMaterial:
    if not color.startswith("#"):
        color = "#" + color

    return ShaderMaterial(vertexShader=COMPARTMENT_VERTEX_SHADER,
                          fragmentShader=COMPARTMENT_FRAGMENT_SHADER,
                          transparent=True, depthTest=True, depthWrite=False,
                          side="DoubleSide", uniforms=dict(color={"value": color}))


class CompartmentViewer(BaseViewer):
    def __init__(self, container: Container,
                 penetration_groups: Union[PenetrationGroup, Iterable[PenetrationGroup]],
                 do_show: bool = True,
                 **kwargs):
        """Container for 3D compartment display widget.

        Parameters
        ----------
        container : Container
        penetration_groups : PenetrationGroup or iterable of PenetrationGroup
        do_show : bool
        kwargs : Keyword arguments
        """
        self._aspect_ratio = 1  # make sure we have a square workspace
        super().__init__(container, penetration_groups, **kwargs)

        # if len(self._penetration_groups) > 1:
        #     raise ValueError("only a single penetration group is supported at this time")

        self._compartment_views = {}

        # construct scene
        self._scene = Scene(children=[AmbientLight("white", 0.3),
                                      DirectionalLight(position=(1, 0, 0))])
        self._camera = PerspectiveCamera(near=550, far=550000,
                                         position=CAMERA_START_POSITION,
                                         quaternion=CAMERA_START_QUATERNION,
                                         rotation=CAMERA_START_ROTATION,
                                         up=(0, -1, 0))
        self._renderer = Renderer(scene=self._scene, camera=self._camera,
                                  controls=[OrbitControls(controlling=self._camera)],
                                  width=self.width, height=self.height)
        self._renderer.layout.border = "solid 1px"

        # add penetration groups to view
        self._views = []
        self._points_by_compartment = defaultdict(dict)
        for pen_group in self.penetration_groups:
            # assign point IDs to compartments
            for p in pen_group.points:
                cid = self.container.find_compartment_by_point(p)
                if pen_group.id not in self._points_by_compartment[cid]:
                    self._points_by_compartment[cid][pen_group.id] = []

                self._points_by_compartment[cid][pen_group.id].append(p.id)

            # construct ThreeViews while we're here
            tv = pen_group.to_three_view()
            self._views.append(tv)

        # add compartments
        for cid in self._points_by_compartment:
            self.add_compartment(cid)

        # visualization tweaks
        self._size_scale = 1.
        self._size_scale_slider = ipywidgets.FloatSlider(value=self._size_scale, min=0.1, max=5.05, step=0.1,
                                                         description="Radius scale", disabled=False,
                                                         readout_format=".2f", continuous_update=False,
                                                         orientation="horizontal", readout=True)
        self._size_scale_slider.observe(self._on_change_size_scale, names="value")

        # animation
        self._t = None
        self._t_value_label = ipywidgets.Label(value="0")
        self._frame_rate_slider = ipywidgets.IntSlider(value=60, min=1, max=120, step=1, description="Frames/sec",
                                                       disabled=True, continuous_update=False, orientation="horizontal",
                                                       readout=True)
        self._play_button = ipywidgets.Play(value=0, min=0, max=1., step=1, description="Press play", disabled=True)
        self._play_button.interval = 17  # ~ 60fps
        self._play_slider = ipywidgets.IntSlider(value=0, min=0, max=1, step=1, disabled=True, readout=False)

        if any(v.action is not None for v in self._views):
            t = np.unique(np.concatenate([view.t for view in self._views]))
            self._dt = np.diff(t).min() / 4
            self._t = np.arange(t.min(), t.max() + self._dt / 2, self._dt)
            self._t_offset = -self._t.min()

            if len(self._views) == 1:
                self._action = self._views[0].action
                self._group = self._views[0].group
            else:  # combine tracks
                tracks = []
                children = []
                for v in self._views:
                    tracks += v.action.clip.tracks
                    children += v.group.children
                clip = AnimationClip(tracks=tracks)
                self._group = Group(children=children)
                self._action = AnimationAction(AnimationMixer(self._group), clip=clip, localRoot=self._group)

            self._scene.add(self._group)

            self._play_button.observe(self._animate, names="value")
            self._frame_rate_slider.observe(self._on_change_frame_rate, names="value")
            self._play_slider.disabled = self._play_button.disabled = self._frame_rate_slider.disabled = False

            self._play_slider.min = self._play_button.min = 0
            self._play_slider.max = self._play_button.max = self._t.size - 1  # right-closed interval
            self._play_slider.step = self._play_button.step = 1

            # show epoch slider
            epochs = self.penetration_groups[0].epochs
            if len(epochs) > 0:
                # glyphs for each epoch (alternate light grey w/ dark grey)
                offsets = [e["start time"] for e in epochs]
                dw = [epochs[i + 1]["start time"] - epochs[i]["start time"]
                      for i in range(len(epochs) - 1)] + [self._t[-1] - epochs[-1]["start time"]]
                egs = {"image": [np.array([[183 - 94 * (i % 2), 183 - 94 * (i % 2), 183 - 94 * (i % 2), 255]],
                                          dtype=np.uint8).view(np.uint32) for i in range(len(epochs))],
                       "x": offsets,
                       "dw": dw
                       }

                # also create labels
                epoch_labels = LabelSet(x="x", y="y", text="names", y_offset=2, text_color="white",
                                        source=ColumnDataSource(dict(
                                            x=[offsets[i] + dw[i] / 4 for i in range(len(epochs))],
                                            y=[0] * len(epochs),
                                            names=[e["id"] for e in epochs])
                                        ))
            else:
                egs = {"image": [], "x": [], "dw": []}
                epoch_labels = None

            egs["image"] += [np.array([[4278190335]], dtype=np.uint32)]
            egs["x"] += [self._t[0]]
            egs["dw"] += [0]

            self._epoch_source = ColumnDataSource(
                dict(
                    image=egs["image"], x=egs["x"], y=[0] * (1 + len(epochs)),
                    dw=egs["dw"], dh=[5] * len(epochs) + [0.5]
                )
            )

            self._epoch_slider_fig = figure(plot_width=self.width, plot_height=50,
                                            x_range=(self._t[0], self._t[-1]), y_range=(0, 5),
                                            tools=[], toolbar_location=None)

            self._epoch_slider_fig.yaxis.visible = False
            self._epoch_slider_fig.xgrid.visible = self._epoch_slider_fig.ygrid.visible = False
            self._epoch_slider_fig.image_rgba(image="image", x="x", y="y", dw="dw", dh="dh", source=self._epoch_source)
            if epoch_labels is not None:
                self._epoch_slider_fig.add_layout(epoch_labels)
            else:
                self._epoch_slider_fig.visible = False

            self._layout = layout([row([self._epoch_slider_fig])])

        try:
            whole_brain = [k for k in self.container.brain_areas
                           if self.container.brain_areas[k]["name"] == "root"][0]
            self.add_compartment(whole_brain)
        except IndexError:  # raise other exceptions
            pass  # no "whole brain" to show, try to carry on without it

        # multi-select list of available brain areas
        loaded_compartments = self.loaded_compartments_by_name
        self._compartment_select = ipywidgets.SelectMultiple(options=loaded_compartments,
                                                             value=[lc[1] for lc in loaded_compartments],
                                                             rows=min(len(loaded_compartments), 10))
        self._compartment_select.observe(self._on_change_selected_compartments, names="value")

        # multi-select list of available penetration groups
        group_names = [(pg.id, i) for i, pg in enumerate(self._penetration_groups)]
        self._pen_group_select = ipywidgets.SelectMultiple(options=group_names,
                                                           value=[gn[1] for gn in group_names],
                                                           rows=min(len(group_names), 10))
        self._pen_group_select.observe(self._on_change_selected_groups, names="value")
        show_all = ipywidgets.Button(description="Select all groups")
        show_all.on_click(self._on_click_show_all)

        # link slider and play button
        ipywidgets.jslink((self._play_button, 'value'), (self._play_slider, 'value'))

        # container for all widgets
        self._workspace = ipywidgets.VBox([
            ipywidgets.HBox([self._play_button, self._t_value_label, self._play_slider]),
            ipywidgets.VBox([self._size_scale_slider, self._frame_rate_slider])
        ], layout=ipywidgets.Layout(display="flex", flex_flow="column"))

        if do_show:
            display(ipywidgets.HBox([
                self.renderer,
                ipywidgets.VBox([
                    ipywidgets.Label("Compartments"),
                    self._compartment_select,
                    ipywidgets.Label("Penetration groups"),
                    self._pen_group_select,
                    show_all
                ])
            ]))
            self._handle = show(self._layout, notebook_handle=True)
            display(self._workspace)

    def _animate(self, change):
        frame = change.new

        t = self._t[frame]
        self._t_value_label.value = f"{t:0.2f}"

        self._action.time = t + self._t_offset
        self._action.play()
        self._action.pause()

        new_epoch_data = {"image": self._epoch_source.data["image"],
                          "x": self._epoch_source.data["x"], "y": self._epoch_source.data["y"],
                          "dw": self._epoch_source.data["dw"][:-1] + [frame * self._dt],
                          "dh": self._epoch_source.data["dh"]}
        self._epoch_source.stream(new_epoch_data, rollover=1 + len(self.penetration_groups[0].epochs))

        push_notebook(handle=self._handle)

    def _on_change_frame_rate(self, change):
        self._play_button.interval = int(1000/change.new)

    def _on_change_selected_compartments(self, change):
        """Add/remove visible compartments from selection widget.

        Parameters
        ----------
        change
        """
        self._compartment_select.disabled = True

        old_vals = np.array(change.old)
        new_vals = np.array(change.new)

        # hide old compartments
        to_hide = np.setdiff1d(old_vals, new_vals)
        for area_id in to_hide:
            self.hide_compartment(area_id)

        # add new compartments
        to_add = np.setdiff1d(new_vals, old_vals)
        if to_add.size > 0:
            for i, area_id in enumerate(to_add):
                self.add_compartment(area_id)

        self._compartment_select.disabled = False

    def _on_change_selected_groups(self, change):
        self._pen_group_select.disabled = True

        old_vals = np.array(change.old)
        new_vals = np.array(change.new)

        # hide old compartments
        to_hide = np.setdiff1d(old_vals, new_vals)
        self.hide_penetration_groups(to_hide)

        # add new compartments
        to_add = np.setdiff1d(new_vals, old_vals)
        self.show_penetration_groups(to_add)

        self._pen_group_select.disabled = False

    def _on_change_size_scale(self, change):
        scale_factor = change.new/change.old

        for child in self._group.children:
            child.material.size *= scale_factor

        tracks = self._action.clip.tracks
        for track in tracks:
            if not track.name.endswith("size"):
                continue
            track.values *= scale_factor

        clip = AnimationClip(tracks=tracks)
        self._action = AnimationAction(AnimationMixer(self._group), clip=clip, localRoot=self._group)

        if not self._play_button._playing:
            self._animate(DummyChange(self._play_button.value))

    def _on_click_show_all(self, btn):
        self._pen_group_select.value = [gn[1] for gn in self._pen_group_select.options]

    def add_compartment(self, compartment_id: int):
        """Add compartment to view by area id, loading from the Allen API if not cached.

        Parameters
        ----------
        compartment_id : int
            Ids of compartments to add.
        """
        assert isinstance(compartment_id, Integral)

        if compartment_id not in self.container.brain_areas:
            raise ValueError(f"compartment '{compartment_id}' not found")

        if compartment_id not in self.compartment_views:
            compartment = self.container.download_compartment_obj(compartment_id)

            shader = _shader_factory(rgb2hex([b / 255 for b in self.container.annotation_colormap[compartment_id]]))
            compartment.children[0].material = shader

            self.compartment_views[compartment_id] = compartment
            self.scene.add(compartment)

        self.show_compartment(compartment_id)

    def show_compartment(self, compartment_id: int):
        """Set a compartment to visible.

        Parameters
        ----------
        compartment_id : int
            ID of compartment to display.
        """
        if not isinstance(compartment_id, Integral):
            raise TypeError(f"compartment_id should be an int, but you gave '{type(compartment_id)}'")

        if compartment_id not in self.container.brain_areas:
            raise ValueError(f"compartment '{compartment_id}' not found")

        if compartment_id not in self.compartment_views:
            self.add_compartment(compartment_id)

        self.compartment_views[compartment_id].visible = True

        # show all points in this compartment
        for pen_group, group_points in self._points_by_compartment[compartment_id].items():
            i_group = self._reverse_pg_map[pen_group]
            if not self._views[i_group].group.visible:
                continue

            keys = [f"group{pen_group}-label{point_id}" for point_id in group_points]
            for child in self._views[i_group].group.children:
                if child.name in keys:
                    child.visible = True

    def hide_compartment(self, compartment_id):
        """Set compartment to invisible.

        Parameters
        ----------
        compartment_id : int
            ID of compartment to hide.
        """
        if not isinstance(compartment_id, Integral):
            raise TypeError(f"compartment_id should be an int, but you gave '{type(compartment_id)}'")

        if compartment_id not in self.compartment_views:
            return

        self.compartment_views[compartment_id].visible = False

        # hide all points in this compartment
        for pen_group, group_points in self._points_by_compartment[compartment_id].items():
            i_group = self._reverse_pg_map[pen_group]
            keys = [f"group{pen_group}-label{point_id}" for point_id in group_points]
            for child in self._views[i_group].group.children:
                if child.name in keys:
                    child.visible = False

    def show_penetration_groups(self, group_ids):
        """Set points in a penetration group to visible.

        Parameters
        ----------
        group_ids
        """
        group_names = [self._views[gid].id for gid in group_ids]
        group_re = re.compile(f"group({'|'.join(group_names)})-")
        for point in self._group.children:
            if group_re.match(point.name):
                point.visible = True

    def hide_penetration_groups(self, group_ids):
        """Set points in a penetration group to invisible.

        Parameters
        ----------
        group_id : int
        """
        group_names = [self._views[gid].id for gid in group_ids]
        group_re = re.compile(f"group({'|'.join(group_names)})-")
        for point in self._group.children:
            if group_re.match(point.name):
                point.visible = False

    @property
    def api_endpoint(self):
        return self.container.api_endpoint

    @property
    def compartment_views(self):
        return self._compartment_views

    @property
    def loaded_compartments(self):
        return [k for k in self.compartment_views]

    @property
    def loaded_compartments_by_name(self):
        """Compartment IDs by name, sorted by depth."""
        keys = self.loaded_compartments
        sorted_keys = sorted(keys, key=lambda k: self.container.brain_areas[k]["graph_order"])
        sorted_names = [self.container.brain_areas[k]["name"] for k in sorted_keys]
        return list(zip(sorted_names, sorted_keys))

    @property
    def renderer(self):
        return self._renderer

    @property
    def scene(self):
        return self._scene
