from collections import defaultdict
from json import JSONDecodeError
import os
import os.path as op

from IPython.display import display
import ipywidgets

from activity_viewer.models import APIEndpoint, PenetrationGroup, Container
from activity_viewer.models.penetration_group import from_json_str
from activity_viewer.viewers import PenetrationViewer, SliceViewer


class ActivityViewer:
    DEFAULT_RESOLUTION = 25
    RESOLUTIONS = [10, 25, 50, 100]
    PENETRATION_VIEW = "Penetration view"
    SLICE_VIEW = "Slice view"
    COMPARTMENT_VIEW = "Compartment view"

    def __init__(self, api_endpoint: APIEndpoint = None):
        """Widget master control thingy."""
        try:
            self.api_endpoint = api_endpoint
        except AssertionError:
            self.api_endpoint = APIEndpoint()

        self._penetration_groups = {}
        self._penetration_group_views = defaultdict(dict)
        self._active_view = self.PENETRATION_VIEW

        # load initial containers
        self._containers = {r: None for r in self.RESOLUTIONS}
        self._containers[self.DEFAULT_RESOLUTION] = Container(self.api_endpoint)

        # multi-select list of penetration groups
        self._pen_group_select = ipywidgets.SelectMultiple(options=[], value=[], rows=4, disabled=False,
                                                           continuous_update=False)
        self._pen_group_select.observe(self._on_change_pen_group_select, names="value")

        # progress indicator for long-running operations (e.g., loading compartments)
        self._progress_bar = ipywidgets.IntProgress(value=0, min=0, max=1)
        self._progress_label = ipywidgets.Label("Progress indicator")

        # resolution selection
        self._resolution_select = ipywidgets.RadioButtons(options=list(zip([f"{r} um/px" for r in self.RESOLUTIONS],
                                                                           self.RESOLUTIONS)),
                                                          value=self.DEFAULT_RESOLUTION, disabled=False)
        self._resolution_select.observe(self._on_change_resolution_select, names="value")

        # file upload
        self._file_upload = ipywidgets.FileUpload(accept=".json", multiple=True)
        self._file_upload.observe(self._on_change_file_upload, names="value")

        # views toggle
        self._views_toggle = ipywidgets.ToggleButtons(options=[self.PENETRATION_VIEW,
                                                               self.SLICE_VIEW,
                                                               self.COMPARTMENT_VIEW],
                                                      description="View to display",
                                                      tooltips=["Activity juxtaposed with compartments",
                                                                "Activity on top of slices",
                                                                "Activity in 3D representation of compartments"],
                                                      disabled=False)
        self._views_toggle.observe(self._on_change_views_toggle, names="value")

        # spawn button!
        self._spawn_button = ipywidgets.Button(description="SPAWN VIEWS!", button_style="info", disabled=True)
        self._spawn_button.on_click(self.spawn_views)

        # container for all widgets
        self._workspace = ipywidgets.VBox([
            ipywidgets.HBox([
                ipywidgets.VBox([
                    ipywidgets.HBox([
                        ipywidgets.Label("Select penetration group"),
                        self._pen_group_select,
                        ipywidgets.Label("Slice resolution"),
                        self._resolution_select
                    ]),
                ]),
                ipywidgets.VBox([
                    self._file_upload
                ])
            ]),
            ipywidgets.HBox([
                self._views_toggle,
                self._spawn_button
            ]),
            ipywidgets.HBox([
                self._progress_label,
                self._progress_bar
            ])
        ], layout=ipywidgets.Layout(display="flex", flex_flow="column"))

        display(self._workspace)

    def _on_change_file_upload(self, change):
        """Upload a JSON file containing a penetration group.

        Parameters
        ----------
        change
        """
        self._file_upload.disabled = True
        n_files = len(change.new)

        self._progress_bar.value = 0
        self._progress_bar.max = n_files

        selected = []

        for filename, data in change.new.items():
            self._progress_bar.value += 1
            self._progress_label.value = f"Loading {self._progress_bar.value}/{n_files}..."
            try:
                pen_groups = from_json_str(data["content"])
                for pen_group in pen_groups:
                    self.add_penetration_group(pen_group, do_select=False)
                    selected.append(pen_group.id)
            except JSONDecodeError as e:
                pass  # warn
            except AssertionError:
                pass  # also warn

        self._progress_label.value = "Done!"
        self._file_upload.disabled = False

        # select all uploaded files
        if len(selected) > 0:
            self._pen_group_select.value = tuple(selected)

    def _on_change_pen_group_select(self, change):
        """Add/remove visible penetration groups from selection widget.

        Parameters
        ----------
        change
        """
        selected = list(change.new)
        if len(selected) > 4:  # only allow 4 at a time
            self._pen_group_select.value = selected[:4]
            return

        self._views_toggle.disabled = False
        self._spawn_button.disabled = False

        # hide old views: the nuclear option
        for group_views in self._penetration_group_views.values():
            for old_view in group_views.values():
                old_view.hide()

        self.spawn_views(None)

    def _on_change_resolution_select(self, change):
        """Set the resolution for reference and annotation volumes.

        Parameters
        ----------
        change
        """
        # this can take a while, so disable everything
        def _traverse_disable(ws, disabled):
            if hasattr(ws, "children"):
                for child in ws.children:
                    _traverse_disable(child, disabled)
            else:
                ws.disabled = disabled

        # hide old views: the nuclear option
        for group_views in self._penetration_group_views.values():
            for old_view in group_views.values():
                old_view.hide()

        _traverse_disable(self._workspace, True)

        self._progress_bar.value = 0
        self._progress_bar.max = 1
        self._progress_label.value = "Loading 1/1..."

        resolution = change.new
        if self._containers[resolution] is None:
            self._containers[resolution] = Container(self.api_endpoint)

        self._progress_bar.value += 1
        self._progress_label.value = "Done!"
        _traverse_disable(self._workspace, False)

        self.spawn_views(None)

    def _on_change_views_toggle(self, change):
        """Display a new view of selected penetration groups

        Parameters
        ----------
        change
        """
        old_view = change.old
        for gid in self._pen_group_select.value:
            if old_view in self._penetration_group_views[gid]:
                self._penetration_group_views[gid][old_view].hide()

        self._active_view = change.new

        # hide old views: the nuclear option
        for group_views in self._penetration_group_views.values():
            for old_view in group_views.values():
                old_view.hide()

        self.spawn_views(None)

    def add_penetration_group(self, penetration_group: PenetrationGroup, do_select=True):
        """Add a penetration group.

        Parameters
        ----------
        penetration_group : PenetrationGroup
        do_select : bool
        """
        assert isinstance(penetration_group, PenetrationGroup)
        if penetration_group.id in self._penetration_groups:
            raise ValueError(f"penetration group with id '{penetration_group} already loaded")

        # add penetration group to self
        gid = penetration_group.id
        self._penetration_groups[gid] = penetration_group

        # add penetration group to each instantiated slice container
        for sc in self._containers.values():
            if sc is None:
                continue
            sc.add_penetration_group(penetration_group)

        # update penetration group select widget (also adds play button where appropriate)
        opts = list(self._pen_group_select.options)
        k = (gid, gid)
        if k not in opts:
            opts = tuple(opts + [k])
            self._pen_group_select.options = opts

        # select this group
        if do_select:
            self._pen_group_select.value = k

    def spawn_views(self, change):
        view = self._active_view + (str(self.resolution) if self._active_view != self.COMPARTMENT_VIEW else "")

        if view == self.COMPARTMENT_VIEW:
            print(f"{view} not yet implemented")
            return

        for gid in self._pen_group_select.value:
            if view not in self._penetration_group_views[gid]:
                pg = self._penetration_groups[gid]
                if view.startswith(self.PENETRATION_VIEW):
                    self._penetration_group_views[gid][view] = PenetrationViewer(self.container, pg)
                elif view.startswith(self.SLICE_VIEW):
                    self._penetration_group_views[gid][view] = SliceViewer(self.container, pg)

            self._penetration_group_views[gid][view].show()

    @property
    def api_endpoint(self):
        return self._api_endpoint

    @api_endpoint.setter
    def api_endpoint(self, val):
        assert isinstance(val, APIEndpoint)
        self._api_endpoint = val

    @property
    def cache_dir(self):
        return self.api_endpoint.cache_dir

    @property
    def resolution(self):
        return self._resolution_select.value

    @property
    def container(self):
        res = self._resolution_select.value
        return self._containers[res]
